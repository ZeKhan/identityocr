package com.iservetech.identityocr;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

//import com.google.android.gms.samples.vision.face.facetracker.FaceTrackerActivity;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.gson.Gson;
import com.iserve.newlayout.facetracker.FaceTrackerActivity;
import com.iservetech.documentocr.DocumentScannerActivity;
import com.iservetech.documentocr.MyKad;
import com.iservetech.documentocr.Passport;
//import com.iservetech.pdfviewer.PdfViewerActivity;

import java.io.File;
import java.io.IOException;
import java.util.List;

//import devliving.online.cvscanner.CVScanner;

/**
 * Created by Ze Khan on 26-Dec-17.
 */

public class MainActivity extends AppCompatActivity /*implements CVScanner.ImageProcessorCallback*/ {
    private Activity mActivity;
    public final static String TAG = "IdentityOcr";
    public static String ocrInfo = "";
    private static Gson gson = new Gson();
    public static MyKad mMyKad;
    public static Passport mPassport;
    public static Bitmap faceImg;
    public static boolean isImei = false;
    public static String mImei;
    Bitmap previousFace;

    private DocumentScannerActivity.DocumentScannerListener documentScannerListener = new DocumentScannerActivity.DocumentScannerListener() {
        @Override
        public void onMyKadOcrCompleted(MyKad myKad) {
            mMyKad = myKad;
            myKad.oriImg = null;
            isImei = false;
            if (myKad.faceImg != null) {
                faceImg = Bitmap.createBitmap(myKad.faceImg);
            }
            myKad.faceImg = null;
            Log.d(TAG, "mykad reading complete");
            ocrInfo = gson.toJson(myKad);
            Intent intent = new Intent(mActivity, ResultDisplayActivity.class);
            intent.putExtra("isPassport", false);
            mActivity.startActivity(intent);
        }

        @Override
        public void onPassportOcrCompleted(Passport passport) {
            mPassport = passport;
            passport.oriImg = null;
            isImei = false;
            if (passport.faceImg != null) {
                faceImg = Bitmap.createBitmap(passport.faceImg);
            }
            passport.faceImg = null;
            Log.d(TAG, "passport reading complete");
            ocrInfo = gson.toJson(passport);
            Intent intent = new Intent(mActivity, ResultDisplayActivity.class);
            intent.putExtra("isPassport", true);
            mActivity.startActivity(intent);
        }

        @Override
        public void onImeiOcrCompleted(String imei) {
            isImei = true;
            mImei = imei;
            Intent intent = new Intent(mActivity, ResultDisplayActivity.class);
            intent.putExtra("imei", true);
            mActivity.startActivity(intent);
            Log.d(TAG, imei);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Activity mainActivity = this;
        setContentView(R.layout.activity_main);

        mActivity = this;
        final Gson gson = new Gson();
        final boolean myMethod = true;

        Log.d(TAG, "on create");
        final ImageView imageView = findViewById(R.id.scanned_image);

        Button myKadButton = findViewById(R.id.scanMyKadButton);
        Button passportButton = findViewById(R.id.scanPassportButton);
        Button imeiButton = findViewById(R.id.scanImeiButton);
        Button faceRecogButton = findViewById(R.id.faceRecogButton);
        Button pdfButton = findViewById(R.id.pdfButton);

        myKadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myMethod) {
                    DocumentScannerActivity.readMyKad(mActivity, documentScannerListener);
                } else {
                    //CVScanner.startScanner(mainActivity, false, 99);
                }
            }
        });

        passportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myMethod) {
                    DocumentScannerActivity.readPassport(mActivity, documentScannerListener);
                } else {
                    //CVScanner.startScanner(mainActivity, true, 99);
                }
            }
        });

        imeiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DocumentScannerActivity.readIMEI(mainActivity, documentScannerListener);
            }
        });

        faceRecogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*FaceTrackerActivity.startFaceValidation(mActivity, new FaceTrackerActivity.onFaceValidatedListener() {
                    @Override
                    public void onFaceValidated(Bitmap bitmap) {
                        Log.d(TAG, "bitmap obtained");
                        //imageView.setImageBitmap(bitmap);

                        FaceTrackerActivity.faceRecognition(mActivity, previousFace, bitmap);
                        previousFace = bitmap;
                    }

                    @Override
                    public void onFaceMatched(boolean success, String message) {

                    }
                });*/
                FaceTrackerActivity.startFaceTracking(MainActivity.this, new FaceTrackerActivity.onFaceValidatedListener() {
                    @Override
                    public void onFaceValidated(Bitmap bitmap) {
                        Log.d(TAG, "bitmap obtained");
                    }

                    @Override
                    public void onFaceMatched(boolean success, String message) {
                        Log.d(TAG, "face matched");
                    }
                });
                /*Intent intent = new Intent(MainActivity.this, FaceTrackerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
            }
        });

        pdfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //PdfViewerActivity.openPdf(MainActivity.this, new File(getExternalFilesDir(null),"WhatsApp-Security-Whitepaper.pdf"));
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "main activity stopped");
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 99 && resultCode == RESULT_OK) {
            String path = data.getStringExtra(CVScanner.RESULT_IMAGE_PATH);

            Log.d(TAG, "Activity result: " + path);

            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);
            Matrix matrix = new Matrix();
            int orientation = 0;
            try {
                ExifInterface exif = new ExifInterface(path);
                orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                switch (orientation) {
                    case 0:
                        matrix.postRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.postRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.postRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.postRotate(270);
                }

                Log.d(TAG, "Orientation: " + String.valueOf(orientation));
            } catch (IOException e) {
                Log.e(TAG, "Exif error: " + e);
            }
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            ImageView view = findViewById(R.id.scanned_image);
            view.setScaleType(ImageView.ScaleType.CENTER);
            view.setImageBitmap(bitmap);

            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            TextRecognizer textRecognizer = new TextRecognizer.Builder(this).build();
            SparseArray<TextBlock> items = textRecognizer.detect(frame);
            for (int i = 0; i < items.size(); i++) {
                TextBlock item = items.valueAt(i);
                List<? extends Text> texts = item.getComponents();
                for (int j = 0; j < texts.size(); j++) {
                    Log.d(TAG, texts.get(j).getValue());
                }
                Log.d(TAG, item.getValue());
            }
        }
    }

    @Override
    public void onImageProcessed(String imagePath) {
        Log.d(TAG, "Image processed: " + imagePath);
    }

    @Override
    public void onImageProcessingFailed(String reason, @Nullable Exception error) {
        Log.d(TAG, "Image processing failed: " + reason);
    }*/
}
