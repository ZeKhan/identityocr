package com.iservetech.identityocr;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

//import com.iservetech.documentocr.MyKadImageProcessor;
import com.iservetech.documentocr.MyKadOcrProcessor;
import com.iservetech.documentocr.PassportOcrProcessor;

/**
 * Created by Ze Khan on 05-Jan-18.
 */

public class Tab2Fragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.info_region_layout, container, false);

        ImageView icNumView = rootView.findViewById(R.id.ic_num_region);
        ImageView nameView = rootView.findViewById(R.id.ic_name_region);
        ImageView addressView = rootView.findViewById(R.id.ic_address_region);
        ImageView genderView = rootView.findViewById(R.id.ic_gender_region);

        if(!ResultDisplayActivity.isPassport){
            icNumView.setImageBitmap(MyKadOcrProcessor.icNumBitmap);
            nameView.setImageBitmap(MyKadOcrProcessor.icNameBitmap);
            addressView.setImageBitmap(MyKadOcrProcessor.icAddBitmap);
            genderView.setImageBitmap(MyKadOcrProcessor.icGenderBitmap);
        }else {
            icNumView.setImageBitmap(PassportOcrProcessor.mrzBitmap);
            nameView.setVisibility(View.INVISIBLE);
            addressView.setVisibility(View.INVISIBLE);
            genderView.setVisibility(View.INVISIBLE);
        }

        return rootView;
    }
}
