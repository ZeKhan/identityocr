package com.iservetech.identityocr;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iserve.newlayout.texttracker.CameraFunction;
import com.iservetech.documentocr.ImeiOcrProcessor;
//import com.iservetech.documentocr.MyKadImageProcessor;

/**
 * Created by Ze Khan on 05-Jan-18.
 */

public class Tab3Fragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ocr_result_layout, container, false);

        TextView ocrView = rootView.findViewById(R.id.ocr_result_text);
        ImageView imageView = rootView.findViewById(R.id.faceImg);
        if(MainActivity.isImei){
            ocrView.setText(MainActivity.mImei);
            imageView.setImageBitmap(ImeiOcrProcessor.mBitmap);
        }else {
            ocrView.setText(MainActivity.ocrInfo);
            imageView.setImageBitmap(MainActivity.faceImg);
        }


        return rootView;
    }
}
