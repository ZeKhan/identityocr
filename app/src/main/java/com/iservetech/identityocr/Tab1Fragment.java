package com.iservetech.identityocr;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

//import com.iservetech.documentocr.ImageProcessor;
//import com.iservetech.documentocr.MyKadImageProcessor;

/**
 * Created by Ze Khan on 05-Jan-18.
 */

public class Tab1Fragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.image_processing_layout, container, false);;

        ImageView cannyEdgeView = rootView.findViewById(R.id.canny_edge);
        ImageView convexHullView = rootView.findViewById(R.id.convex_hull);
        ImageView houghLinesView = rootView.findViewById(R.id.hought_line);
        ImageView perspectiveView = rootView.findViewById(R.id.perspective);

        //cannyEdgeView.setImageBitmap(ImageProcessor.cannyBitmap);
        //convexHullView.setImageBitmap(ImageProcessor.outlineBitmap);
        //houghLinesView.setImageBitmap(ImageProcessor.houghLinesBitmap);
        //perspectiveView.setImageBitmap(ImageProcessor.perspectiveBitmap);

        return rootView;
    }
}
