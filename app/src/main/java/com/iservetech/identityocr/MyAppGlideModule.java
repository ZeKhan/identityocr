package com.iservetech.identityocr;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Ze Khan on 26-Dec-17.
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {
}
