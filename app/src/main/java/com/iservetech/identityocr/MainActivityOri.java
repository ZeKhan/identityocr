package com.iservetech.identityocr;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class MainActivityOri extends AppCompatActivity {
    // Used to load the 'native-lib' library on application startup.
    /*static {
        System.loadLibrary("native-lib");
    }*/
    final String TAG = "tesseract";

    Bitmap image;
    private TessBaseAPI mTess;
    String datapath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ori);

        // Example of a call to a native method
        /*TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());*/

        //image = BitmapFactory.decodeResource(getResources(), R.drawable.letters);
        int imageInd = R.drawable.passport;

        GlideApp.with(this).asBitmap().load(imageInd).fitCenter().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                Log.d(TAG, "Bitmap loaded");
                image = resource;
            }
        });

        ImageView imageView = findViewById(R.id.imageView);
        imageView.setImageResource(imageInd);
        //imageView.setBackgroundResource(imageInd);
        datapath = getFilesDir() + "/tesseract/";
        Log.d(TAG, getFilesDir().getPath());
        checkFile(new File(datapath + "tessdata/"));

        String lang = "eng";
        mTess = new TessBaseAPI();
        mTess.init(datapath, lang);
    }

    private void checkFile(File dir){
        if (!dir.exists()&& dir.mkdirs()){
            copyFiles();
        }
        //The directory exists, but there is no data file in it
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/eng.traineddata";
            File datafile = new File(datafilepath);
            if (!datafile.exists()) {
                copyFiles();
            }
        }
    }

    public void processImage(View view){
        Log.d(TAG, "Processing image");
        Frame frame = new Frame.Builder().setBitmap(image).build();
        TextRecognizer ocrProcessor = new TextRecognizer.Builder(this).build();
        SparseArray<TextBlock> items = ocrProcessor.detect(frame);
        for(int i = 0; i < items.size(); i++){
            TextBlock item = items.valueAt(i);
            List<? extends Text> texts = item.getComponents();
            for(int j = 0; j < texts.size(); j++){
                Log.d(TAG, texts.get(j).getValue());
            }
            Log.d(TAG, item.getValue());
        }
        /*String ocrResult = null;
        mTess.setImage(image);
        ocrResult = mTess.getUTF8Text();
        Log.d(TAG, ocrResult);
        TextView ocrTextView = (TextView) findViewById(R.id.OCRTextView);
        ocrTextView.setText(ocrResult);*/
    }

    private void copyFiles(){
        try {
            //location we want the file to be at
            String filepath = datapath + "/tessdata/eng.traineddata";

            //get access to AssetManager
            AssetManager assetManager = getAssets();

            //open byte streams for reading/writing
            InputStream instream = assetManager.open("tessdata/eng.traineddata");
            OutputStream outstream = new FileOutputStream(filepath);

            //copy the file to the location specified by filepath
            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }
            outstream.flush();
            outstream.close();
            instream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    //public native String stringFromJNI();
}
