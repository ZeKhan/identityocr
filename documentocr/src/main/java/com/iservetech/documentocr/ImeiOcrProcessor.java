package com.iservetech.documentocr;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.features2d.BOWImgDescriptorExtractor;
import org.opencv.imgproc.Imgproc;

import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

/**
 * Created by Ze Khan on 19 January 2018.
 */

public class ImeiOcrProcessor extends OcrProcessor {
    public static Bitmap mBitmap;
    public static String performOcr(Bitmap bitmap) {
        Log.d(TAG, "bitmap width: " + bitmap.getWidth());
        double ratio = 800f / bitmap.getWidth();
        bitmap = Bitmap.createScaledBitmap(bitmap, (int) (ratio * bitmap.getWidth()), (int) (ratio * bitmap.getHeight()), false);
        Mat mat = new Mat();
        Utils.bitmapToMat(bitmap, mat);
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
        //Imgproc.threshold(mat, mat, 120, 255, Imgproc.THRESH_BINARY);
        //Imgproc.adaptiveThreshold(mat, mat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
        Imgproc.GaussianBlur(mat, mat, new Size(3, 3), 0);
        Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        //Imgproc.erode(mat, mat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5,5)));
        Utils.matToBitmap(mat, bitmap);
        mBitmap = bitmap;
        //TesseractOcr.processImage(bitmap, DocumentScannerActivity.ocrDetectionCompleteListener);
        mat.release();
        String imei = "";
        TextRecognizer textRecognizer = new TextRecognizer.Builder(DocumentScannerActivity.mContext).build();
        Frame frame = new Frame.Builder().setBitmap(bitmap).build();
        SparseArray<TextBlock> textBlockSparseArray = textRecognizer.detect(frame);

        outerMostLoop:
        for (int i = 0; i < textBlockSparseArray.size(); i++) {
            TextBlock textBlock = textBlockSparseArray.valueAt(i);
            Log.d(TAG, "text block values: " + textBlock.getValue());
            for (int j = 0; j < textBlock.getComponents().size(); j++) {
                String line = similarLetters2Digits(textBlock.getComponents().get(j).getValue().replace(" ",""));
                imei = validateImei(line);

                if(!imei.equals(""))
                    break outerMostLoop;

                int digits = 0;
                for (int k = 0; k < line.length(); k++) {
                    if (Character.isDigit(line.charAt(k))) {
                        digits++;
                    }
                }
                if (digits > line.length() / 2 && digits > 10) {
                    try {
                        if (line.length() >= 15) {
                            //imei = line.substring(line.length() - 15, line.length());
                        } else {
                            //imei = line;
                        }
                    } catch (StringIndexOutOfBoundsException e) {
                        //imei = line;
                        e.printStackTrace();
                    }
                    break outerMostLoop;
                }
            }
        }

        imei = imei.toUpperCase();
        imei = similarLetters2Digits(imei).replace(" ", "");
        Log.d(TAG, "imei: " + imei);

        return imei;
    }

    private static String validateImei(String string) {
        if(string.length() < 15)
            return "";

        try {
            Log.d(TAG, "validating imei string: " + string);
            for (int i = 0; i < string.length(); i++) {
                Log.d(TAG, "imei string char at " + i + ": " + string.charAt(i));
                if (Character.isDigit(string.charAt(i)) && string.length() - i >= 15) {
                    try {
                        Long.parseLong(string.substring(i, i + 15));
                        Log.d(TAG, "validating imei segment: " + string.substring(i, i + 15));
                    } catch (NumberFormatException e) {
                        continue;
                    }

                    int sum = 0;
                    for (int j = i; j < i + 14; j++) {
                        if ((j - i) % 2 != 0) {
                            int multiplied = Character.getNumericValue(string.charAt(j)) * 2;
                            sum += multiplied / 10;
                            sum += multiplied % 10;
                        } else {
                            sum += Character.getNumericValue(string.charAt(j));
                        }
                    }

                    int checkDigit = sum % 10 == 0 ? 0 : 10 - (sum % 10);
                    Log.d(TAG, "imei check digit is: " + checkDigit);
                    if (Character.getNumericValue(string.charAt(i + 14)) == checkDigit) {
                        Log.d(TAG, "imei validated: " + string.substring(i, i + 15));
                        return string.substring(i, i + 15);
                    }
                }
            }
        }catch (Exception e){

        }



        return "";
    }
}
