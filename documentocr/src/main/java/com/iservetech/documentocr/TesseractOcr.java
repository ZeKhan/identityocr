package com.iservetech.documentocr;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;

import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

/**
 * Created by Ze Khan on 04-Jan-18.
 */

public class TesseractOcr {
    private static String datapath;
    private static String languageFile = "";
    public static TessBaseAPI mTess;
    private static boolean tessInit;
    static OcrDetectionCompleteListener mListener;
    private static ocrDetectionTask mOcrDetectionTask;

    public static void initialiseTesseract() {
        new tesseractInitTask().execute();
    }

    public static void processImage(Bitmap bitmap, OcrDetectionCompleteListener listener) {
        mListener = listener;
        if(mOcrDetectionTask != null && mOcrDetectionTask.getStatus() == AsyncTask.Status.RUNNING) {
            mOcrDetectionTask.cancel(true);
            mOcrDetectionTask = null;
        }

        mOcrDetectionTask = new ocrDetectionTask();
        mOcrDetectionTask.execute(bitmap);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if(mOcrDetectionTask.getStatus() != AsyncTask.Status.FINISHED) {
                    Log.d(TAG, "ocr cancelled");
                    mOcrDetectionTask.cancel(true);
                    mListener.onOcrDetectionComplete("");
                }
            }
        },5000);
    }

    private static void checkFile(File dir) {
        if (!dir.exists() && dir.mkdirs()) {
            Log.d(TAG, "Directory does not exists");
            copyFiles();
        }
        //The directory exists, but there is no data file in it
        if (dir.exists()) {
            String datafilepath = datapath + "tessdata/" + languageFile + ".traineddata";
            File datafile = new File(datafilepath);
            if (!datafile.exists()) {
                Log.d(TAG, "Language file does not exist");
                copyFiles();
            }
        }
    }

    private static void copyFiles() {
        if (languageFile.equals(""))
            return;

        try {
            //location we want the file to be at
            String filepath = datapath + "tessdata/" + languageFile + ".traineddata";

            //get access to AssetManager
            AssetManager assetManager = DocumentScannerActivity.mContext.getAssets();

            //open byte streams for reading/writing
            InputStream instream = assetManager.open("tessdata/" + languageFile + ".traineddata");
            OutputStream outstream = new FileOutputStream(filepath);
            //copy the file to the location specified by filepath
            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {

                outstream.write(buffer, 0, read);
            }
            outstream.flush();
            outstream.close();
            instream.close();

        } catch (FileNotFoundException e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    public interface OcrDetectionCompleteListener {
        void onOcrDetectionComplete(String text);
    }

    private static class tesseractInitTask extends AsyncTask<Boolean, String, String> {
        @Override
        protected String doInBackground(Boolean... booleans) {
            tessInit = false;
            datapath = DocumentScannerActivity.mContext.getFilesDir() + "/tesseract/";
            switch (DocumentScannerActivity.documentType) {
                case IMEI:
                    break;
                case MyKad:
                    break;
                case Passport:
                    languageFile = "OCRB";
                    break;
            }
            checkFile(new File(datapath + "tessdata/"));
            if (mTess == null) {
                mTess = new TessBaseAPI();
            } else {
                mTess.end();
            }

            if (!languageFile.equals("")) {
                try {
                    mTess.init(datapath, languageFile);
                    Log.d(TAG, "Tesseract initialised with " + languageFile);
                    languageFile = "";
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }

            tessInit = true;
            return null;
        }
    }

    private static class ocrDetectionTask extends AsyncTask<Bitmap, Integer, String> {
        public static boolean isDone;
        @Override
        protected String doInBackground(Bitmap... bitmaps) {
            Log.d(TAG, "tesseract processing");
            if (mTess == null) {
                initialiseTesseract();
            }
            while (!tessInit) {

            }

            Log.d(TAG,"tesseract performing ocr");
            switch (DocumentScannerActivity.documentType) {
                case IMEI:
                    mTess.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "1234567890");
                    break;
                case MyKad:
                    break;
                case Passport:
                    mTess.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "ABCDEFGHIJKLMNOPQRSTUVWXYZ<1234567890");
                    mTess.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, " ");
                    break;
            }


            mTess.setImage(bitmaps[0]);
            String ocrResult = mTess.getUTF8Text();

            //mListener.onOcrDetectionComplete(ocrResult);

            Log.d(TAG, "ocr completed");
            Log.d(TAG, "ocr result: " + ocrResult);
            return ocrResult;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(!isCancelled()){
                Log.d(TAG, "is not cancelled");
                mListener.onOcrDetectionComplete(s);
            }
        }
    }

}
