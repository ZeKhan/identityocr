package com.iservetech.documentocr;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.iservetech.documentocr.CameraPreviewActivity.cropImage;
import static com.iservetech.documentocr.CameraPreviewActivity.cropView;
import static com.iservetech.documentocr.CameraPreviewActivity.overlayView;
import static com.iservetech.documentocr.CameraPreviewActivity.setOptimalCropSize;
import static com.iservetech.documentocr.DocumentScannerActivity.TAG;
import static com.iservetech.documentocr.DocumentScannerActivity.documentType;

/**
 * This class assumes the parent layout is RelativeLayout.LayoutParams.
 */
public class CameraPreview extends TextureView implements SurfaceHolder.Callback, View.OnTouchListener {
    private static boolean DEBUGGING = true;
    private static final String LOG_TAG = "CameraPreviewSample";
    private static final String CAMERA_PARAM_ORIENTATION = "orientation";
    private static final String CAMERA_PARAM_LANDSCAPE = "landscape";
    private static final String CAMERA_PARAM_PORTRAIT = "portrait";
    protected Activity mActivity;
    private TextureView mPreview;
    private SurfaceHolder mHolder;
    protected static Camera mCamera;
    protected Camera.Parameters cameraParams;
    protected List<Camera.Size> mPreviewSizeList;
    protected List<Camera.Size> mPictureSizeList;
    static Camera.Size mPreviewSize;
    static Camera.Size mPictureSize;
    private int mSurfaceChangedCallDepth = 0;
    private int mCameraId;
    private LayoutMode mLayoutMode;
    private int mCenterPosX = -1;
    private int mCenterPosY;

    private int mRatioWidth = 0;
    private int mRatioHeight = 0;

    PreviewReadyCallback mPreviewReadyCallback = null;

    Camera.ShutterCallback onShutterCallback = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {

        }
    };

    Camera.PictureCallback onPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] bytes, Camera camera) {

        }
    };

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Camera.Parameters params = mCamera.getParameters();
        if (mCamera != null && motionEvent.getAction() == MotionEvent.ACTION_DOWN && params.getMaxNumFocusAreas() > 0) {
            try {
                Camera.Size previewSize = params.getPreviewSize();
                Log.d(TAG, "touched");
                int rectSize = 100;//(int)(mPreviewSize.width * 0.1);
                //Rect focusRect = new Rect(previewSize.width / 2 - rectSize, previewSize.height / 2 - rectSize, previewSize.width / 2 + rectSize, previewSize.height / 2 + rectSize);
                int width = (int) ((double) cropView.getMeasuredWidth() / overlayView.getMeasuredWidth() * 2000);
                int height = (int) ((double) cropView.getMeasuredHeight() / overlayView.getMeasuredHeight() * 2000);
                Rect focusRect = new Rect(-width / 2, -height / 2, width / 2, height / 2);
                Log.d(TAG, "rect: " + focusRect.toString());
                List<Camera.Area> focusList = new ArrayList<>();
                Camera.Area focusArea = new Camera.Area(focusRect, 1000);
                focusList.add(focusArea);

                params.setFocusAreas(focusList);
                //params.setMeteringAreas(focusList);
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                mCamera.cancelAutoFocus();
                mCamera.setParameters(params);
                mCamera.startPreview();

                mCamera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        if (!success) {
                            Log.d(TAG, "focusing failed");
                            Camera.Parameters params = mCamera.getParameters();
                            mCamera.cancelAutoFocus();
                            params.setFocusAreas(null);
                            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                            mCamera.setParameters(params);
                            mCamera.startPreview();
                            //mCamera.autoFocus(null);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public static enum LayoutMode {
        FitToParent, // Scale to the size that no side is larger than the parent
        NoBlank // Scale to the size that no side is smaller than the parent
    }

    ;

    public interface PreviewReadyCallback {
        public void onPreviewReady();
    }

    /**
     * State flag: true when surface's layout size is set and surfaceChanged()
     * process has not been completed.
     */
    protected boolean mSurfaceConfiguring = false;

    public CameraPreview(Activity activity, int cameraId, LayoutMode mode) {
        super(activity); // Always necessary
        mActivity = activity;
        mLayoutMode = mode;
        setSurfaceTextureListener(mSurfaceTextureListener);
        /*mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);*/
        mPreview = this;
        this.setClickable(true);
        setOnTouchListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            if (Camera.getNumberOfCameras() > cameraId) {
                mCameraId = cameraId;
            } else {
                mCameraId = 0;
            }
        } else {
            mCameraId = 0;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            //mCamera = Camera.open(mCameraId);
            mCamera = Camera.open();
        } else {
            mCamera = Camera.open();
        }
        cameraParams = mCamera.getParameters();
        mPreviewSizeList = cameraParams.getSupportedPreviewSizes();
        mPictureSizeList = cameraParams.getSupportedPictureSizes();
    }

    private Camera.Parameters getCameraParams() {
        return mCamera.getParameters();
    }

    public final TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            Log.d("tag", "surface texture available");
            try {
                mCamera.setPreviewTexture(texture);
                mCamera.startPreview();
                cameraParams = getCameraParams();
                cameraParams.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                mCamera.setParameters(cameraParams);
                mSurfaceChangedCallDepth++;
                doSurfaceChanged(width, height);
                mSurfaceChangedCallDepth--;
                ViewGroup.LayoutParams params = cropView.getLayoutParams();
                switch (documentType) {
                    case IMEI:
                        params = setOptimalCropSize(params, 1.2, 0.2, 0.35);
                        break;
                    case MyKad:
                        params = setOptimalCropSize(params, 1.6, 1, 0.10);
                        break;
                    case Passport:
                        params = setOptimalCropSize(params, 1.5, 1, 0.10);
                        break;
                }
                cropView.setLayoutParams(params);
            } catch (Exception e) {
                mCamera.release();
                mCamera = null;
                e.printStackTrace();
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            mSurfaceChangedCallDepth++;
            doSurfaceChanged(width, height);
            mSurfaceChangedCallDepth--;
            Log.d("tag", "surface texture size changed");
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            return true;
        }


        int count = 0;

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
            if (processingDone) {
                processingDone = false;
                final Bitmap bitmap = Bitmap.createBitmap(mPreview.getBitmap());
                new LiveImageProcessingTask().execute(bitmap);
            }
        }
    };

    boolean processingDone = true;

    private class LiveImageProcessingTask extends AsyncTask<Bitmap, Integer, Bitmap> {

        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            if (bitmaps.length < 1)
                return null;

            processingDone = false;
            TextRecognizer textRecognizer = new TextRecognizer.Builder(CameraPreviewActivity.mActivity).build();
            Bitmap bitmap = cropImage(bitmaps[0]);
            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            SparseArray<TextBlock> textBlockSparseArray = textRecognizer.detect(frame);
            List<Text> textList = new ArrayList<>();
            for (int i = 0; i < textBlockSparseArray.size(); i++) {
                TextBlock textBlock = textBlockSparseArray.valueAt(i);
                textList.addAll(textBlock.getComponents());
                //textBlock.getComponents().get(0).
                Log.d(TAG, "text block content: \n" + textBlock.getValue());
            }

            Collections.sort(textList, new Comparator<Text>() {
                @Override
                public int compare(Text text, Text t1) {
                    return Integer.valueOf(text.getBoundingBox().top).compareTo(t1.getBoundingBox().top);
                }
            });

            List<Text> removeList = new ArrayList<>();
            for (int i = 0; i < textList.size(); i++) {
                Text line = textList.get(i);
                if (line.getBoundingBox().left > bitmap.getWidth() * 0.5) {
                    Log.d(TAG, "text on the right removed: " + line.getValue());
                    removeList.add(line);
                    continue;
                }

                if (i > 0) {
                    Text prevLine = textList.get(i - 1);
                    if (Math.abs(line.getBoundingBox().top - prevLine.getBoundingBox().top) < line.getBoundingBox().height() * 0.5 && !removeList.contains(prevLine)) {
                        Log.d(TAG, "overlapping lines detected: \n" + line.getValue() + "\n" + prevLine.getValue());
                        if (line.getValue().length() > prevLine.getValue().length()) {
                            removeList.add(prevLine);
                        } else {
                            removeList.add(line);
                        }
                    }
                }
            }
            textList.removeAll(removeList);

            // get ic number
            boolean icObtained = false;
            String icNum = "";
            outerloop:
            for (int i = 0; i < textList.size(); i++) {
                Text line = textList.get(i);
                String lineString = line.getValue();
                if (line.getBoundingBox().top > 0.5 * bitmap.getHeight()) {
                    break;
                }

                if (lineString.length() != 14) {
                    continue;
                }

                for (int j = 0; j < lineString.length(); j++) {
                    List<Integer> digitsInd = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 7, 8, 10, 11, 12, 13));
                    if (digitsInd.contains(j)) {
                        if (!Character.isDigit(lineString.charAt(j)))
                            continue outerloop;
                    } else {
                        if (lineString.charAt(j) != '-')
                            continue outerloop;
                    }
                }

                icObtained = true;
                //myKad.icNum = lineString;
                Log.d(TAG, "ic num: " + lineString);
            }

            if (!icObtained)
                return null;

            boolean nameAddressObtained = false;
            String name = "";
            String address = "";
            for (int i = 0; i < textList.size(); i++) {
                //Text line = textList.get(i);
                if (textList.get(i).getBoundingBox().top > 0.5 * bitmap.getHeight()) {
                    if (textList.size() - i < 5)
                        break;

                    outerloop:
                    for (int j = i; j < textList.size(); j++) {
                        Text line2 = textList.get(j);
                        for(int k = 0; k < name.length(); k++){
                            if(Character.isDigit(name.charAt(k)) || Character.isLowerCase(name.charAt(k)))
                                break outerloop;
                        }

                        if (j == i) {
                            name = line2.getValue();
                        }else if (j == i + 1){
                            Text prevLine = textList.get(j - 1);
                            /*Log.d(TAG, "line diff: " + Math.abs(line2.getBoundingBox().top - prevLine.getBoundingBox().bottom));
                            Log.d(TAG, "line height: " + line2.getBoundingBox())*/
                            if(Math.abs(line2.getBoundingBox().top - prevLine.getBoundingBox().bottom) < line2.getBoundingBox().height() * 0.5){
                                name += " " + line2.getValue();
                            }else{
                                //myKad.address.add(line2.getValue());
                                address += line2.getValue() + ", ";
                            }
                        }else{
                            //myKad.address.add(line2.getValue());
                            address += line2.getValue();
                            if(j != textList.size()-1){
                                address += ", ";
                            }
                        }
                    }

                    nameAddressObtained = true;
                    //myKad.name = name;
                    Log.d(TAG, "name: " + name);
                    Log.d(TAG, "address: " + address);

                    break;
                }
            }

            if(!nameAddressObtained)
                return null;

            Log.d(TAG, "----------------------------------");

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            processingDone = true;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(mHolder);
            cameraParams = getCameraParams();
            cameraParams.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            mCamera.setParameters(cameraParams);

        } catch (IOException e) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mSurfaceChangedCallDepth++;
        doSurfaceChanged(width, height);
        ViewGroup.LayoutParams params = cropView.getLayoutParams();
        switch (documentType) {
            case IMEI:
                params = setOptimalCropSize(params, 1.2, 0.2, 0.35);
                break;
            case MyKad:
                params = setOptimalCropSize(params, 1.6, 1, 0.10);
                break;
            case Passport:
                params = setOptimalCropSize(params, 1.5, 1, 0.10);
                break;
        }
        cropView.setLayoutParams(params);
        mSurfaceChangedCallDepth--;
    }

    private void doSurfaceChanged(int width, int height) {
        mCamera.stopPreview();

        Camera.Parameters cameraParams = mCamera.getParameters();
        List<Camera.Size> cameraSizeList = cameraParams.getSupportedPreviewSizes();
        boolean portrait = isPortrait();

        // The code in this if-statement is prevented from executed again when surfaceChanged is
        // called again due to the change of the layout size in this if-statement.
        if (!mSurfaceConfiguring) {
            //Camera.Size previewSize = determinePreviewSize(portrait, width, height);
            Point displaySize = new Point();
            mActivity.getWindowManager().getDefaultDisplay().getSize(displaySize);
            int rotatedPreviewWidth = width;
            int rotatedPreviewHeight = height;
            int maxPreviewWidth = displaySize.x;
            int maxPreviewHeight = displaySize.y;

            //Camera.Size previewSize = chooseOptimalSize(cameraSizeList, rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth, maxPreviewHeight, Collections.max(cameraSizeList, new CompareSizesByArea()));
            Camera.Size previewSize = determinePreviewSize(portrait, width, height);
            Camera.Size pictureSize = determinePictureSize(previewSize);
            if (DEBUGGING) {
                Log.v(LOG_TAG, "Desired Preview Size - w: " + width + ", h: " + height);
            }
            mPreviewSize = previewSize;
            mPictureSize = pictureSize;
            mSurfaceConfiguring = adjustSurfaceLayoutSize(previewSize, portrait, width, height);
            // Continue executing this method if this method is called recursively.
            // Recursive call of surfaceChanged is very special case, which is a path from
            // the catch clause at the end of this method.
            // The later part of this method should be executed as well in the recursive
            // invocation of this method, because the layout change made in this recursive
            // call will not trigger another invocation of this method.
            if (mSurfaceConfiguring && (mSurfaceChangedCallDepth <= 1)) {
                return;
            }
        }

        configureCameraParameters(cameraParams, portrait);
        mSurfaceConfiguring = false;

        try {
            mCamera.startPreview();
        } catch (Exception e) {
            Log.w(LOG_TAG, "Failed to start preview: " + e.getMessage());

            // Remove failed size
            mPreviewSizeList.remove(mPreviewSize);
            mPreviewSize = null;

            // Reconfigure
            if (mPreviewSizeList.size() > 0) { // prevent infinite loop
                surfaceChanged(null, 0, width, height);
            } else {
                Toast.makeText(mActivity, "Can't start preview", Toast.LENGTH_LONG).show();
                Log.w(LOG_TAG, "Gave up starting preview");
            }
        }

        if (null != mPreviewReadyCallback) {
            mPreviewReadyCallback.onPreviewReady();
        }
    }

    /**
     * @param portrait
     * @param reqWidth  must be the value of the parameter passed in surfaceChanged
     * @param reqHeight must be the value of the parameter passed in surfaceChanged
     * @return Camera.Size object that is an element of the list returned from Camera.Parameters.getSupportedPreviewSizes.
     */
    protected Camera.Size determinePreviewSize(boolean portrait, int reqWidth, int reqHeight) {
        // Meaning of width and height is switched for preview when portrait,
        // while it is the same as user's view for surface and metrics.
        // That is, width must always be larger than height for setPreviewSize.
        int reqPreviewWidth; // requested width in terms of camera hardware
        int reqPreviewHeight; // requested height in terms of camera hardware
        if (portrait) {
            reqPreviewWidth = reqHeight;
            reqPreviewHeight = reqWidth;
        } else {
            reqPreviewWidth = reqWidth;
            reqPreviewHeight = reqHeight;
        }

        if (DEBUGGING) {
            Log.v(LOG_TAG, "Listing all supported preview sizes");
            for (Camera.Size size : mPreviewSizeList) {
                Log.v(LOG_TAG, "  w: " + size.width + ", h: " + size.height);
            }
            Log.v(LOG_TAG, "Listing all supported picture sizes");
            for (Camera.Size size : mPictureSizeList) {
                Log.v(LOG_TAG, "  w: " + size.width + ", h: " + size.height);
            }
        }

        // Adjust surface size with the closest aspect-ratio
        float reqRatio = ((float) reqPreviewWidth) / reqPreviewHeight;
        float curRatio, deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        Camera.Size retSize = null;
        for (Camera.Size size : mPreviewSizeList) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }

        return retSize;
    }

    /**
     * Sets the aspect ratio for this view. The size of the view will be measured based on the ratio
     * calculated from the parameters. Note that the actual sizes of parameters don't matter, that
     * is, calling setAspectRatio(2, 3) and setAspectRatio(4, 6) make the same result.
     *
     * @param width  Relative horizontal size
     * @param height Relative vertical size
     */
    public void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("Size cannot be negative.");
        }
        mRatioWidth = width;
        mRatioHeight = height;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(LOG_TAG, "on measure called");
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height);
        } else {
            if (width < height * mRatioWidth / mRatioHeight) {
                setMeasuredDimension(width, width * mRatioHeight / mRatioWidth);
            } else {
                setMeasuredDimension(height * mRatioWidth / mRatioHeight, height);
            }
        }
    }

    public static Camera.Size chooseOptimalSize(List<Camera.Size> choices, int textureViewWidth,
                                                int textureViewHeight, int maxWidth, int maxHeight, Camera.Size aspectRatio) {

        List<Camera.Size> bigEnough = new ArrayList<>();
        List<Camera.Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.width;
        int h = aspectRatio.height;
        for (Camera.Size option : choices) {
            if (option.width <= maxWidth && option.height <= maxHeight &&
                    option.height == option.width * h / w) {
                if (option.width >= textureViewWidth &&
                        option.height >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            return choices.get(0);
        }
    }

    static class CompareSizesByArea implements Comparator<Camera.Size> {

        @Override
        public int compare(Camera.Size lhs, Camera.Size rhs) {
            return Long.signum((long) lhs.width * lhs.height - (long) rhs.width * rhs.height);
        }

    }

    protected Camera.Size determinePictureSize(Camera.Size previewSize) {
        Camera.Size retSize = null;
        for (Camera.Size size : mPictureSizeList) {
            if (size.equals(previewSize)) {
                return size;
            }
        }

        if (DEBUGGING) {
            Log.v(LOG_TAG, "Same picture size not found.");
        }

        // if the preview size is not supported as a picture size
        float reqRatio = ((float) previewSize.width) / previewSize.height;
        float curRatio, deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        for (Camera.Size size : mPictureSizeList) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }

        return retSize;
    }

    protected boolean adjustSurfaceLayoutSize(Camera.Size previewSize, boolean portrait,
                                              int availableWidth, int availableHeight) {
        float tmpLayoutHeight, tmpLayoutWidth;
        if (portrait) {
            tmpLayoutHeight = previewSize.width;
            tmpLayoutWidth = previewSize.height;
        } else {
            tmpLayoutHeight = previewSize.height;
            tmpLayoutWidth = previewSize.width;
        }

        float factH, factW, fact;
        factH = availableHeight / tmpLayoutHeight;
        factW = availableWidth / tmpLayoutWidth;
        if (mLayoutMode == LayoutMode.FitToParent) {
            // Select smaller factor, because the surface cannot be set to the size larger than display metrics.
            if (factH < factW) {
                fact = factH;
                //fact = factW;
            } else {
                fact = factW;
                //fact = factH;
            }
        } else {
            if (factH < factW) {
                fact = factW;
                //fact = factH;
            } else {
                fact = factH;
                //fact = factW;
            }
        }

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.getLayoutParams();

        int layoutHeight = (int) (tmpLayoutHeight * fact);
        int layoutWidth = (int) (tmpLayoutWidth * fact);
        if (DEBUGGING) {
            Log.v(LOG_TAG, "Preview Measured Size: " + this.getMeasuredWidth() + "/" + this.getMeasuredHeight());
            Log.v(LOG_TAG, "Preview Layout Size - w: " + layoutWidth + ", h: " + layoutHeight);
            Log.v(LOG_TAG, "Scale factor: " + fact);
        }

        boolean layoutChanged;
        if ((layoutWidth != this.getWidth()) || (layoutHeight != this.getHeight())) {
            layoutParams.height = layoutHeight;
            layoutParams.width = layoutWidth;
            if (mCenterPosX >= 0) {
                layoutParams.topMargin = mCenterPosY - (layoutHeight / 2);
                layoutParams.leftMargin = mCenterPosX - (layoutWidth / 2);
            }
            this.setLayoutParams(layoutParams); // this will trigger another surfaceChanged invocation.
            layoutChanged = true;
        } else {
            layoutChanged = false;
        }

        return layoutChanged;
    }

    /**
     * @param x X coordinate of center position on the screen. Set to negative value to unset.
     * @param y Y coordinate of center position on the screen.
     */
    public void setCenterPosition(int x, int y) {
        mCenterPosX = x;
        mCenterPosY = y;
    }

    protected void configureCameraParameters(Camera.Parameters cameraParams, boolean portrait) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) { // for 2.1 and before
            if (portrait) {
                cameraParams.set(CAMERA_PARAM_ORIENTATION, CAMERA_PARAM_PORTRAIT);
            } else {
                cameraParams.set(CAMERA_PARAM_ORIENTATION, CAMERA_PARAM_LANDSCAPE);
            }
        } else { // for 2.2 and later
            int angle;
            Display display = mActivity.getWindowManager().getDefaultDisplay();
            switch (display.getRotation()) {
                case Surface.ROTATION_0: // This is display orientation
                    angle = 90; // This is camera orientation
                    break;
                case Surface.ROTATION_90:
                    angle = 0;
                    break;
                case Surface.ROTATION_180:
                    angle = 270;
                    break;
                case Surface.ROTATION_270:
                    angle = 180;
                    break;
                default:
                    angle = 90;
                    break;
            }
            Log.v(LOG_TAG, "angle: " + angle);
            mCamera.setDisplayOrientation(angle);
        }

        cameraParams.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
        cameraParams.setPictureSize(mPictureSize.width, mPictureSize.height);
        if (DEBUGGING) {
            Log.v(LOG_TAG, "Preview Actual Size - w: " + mPreviewSize.width + ", h: " + mPreviewSize.height);
            Log.v(LOG_TAG, "Picture Actual Size - w: " + mPictureSize.width + ", h: " + mPictureSize.height);
        }

        mCamera.setParameters(cameraParams);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stop();
    }

    public void stop() {
        if (null == mCamera) {
            return;
        }
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    public boolean isPortrait() {
        return (mActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }

    public void setOneShotPreviewCallback(PreviewCallback callback) {
        if (null == mCamera) {
            return;
        }
        mCamera.setOneShotPreviewCallback(callback);
    }

    public void setPreviewCallback(PreviewCallback callback) {
        if (null == mCamera) {
            return;
        }
        mCamera.setPreviewCallback(callback);
    }

    public Camera.Size getPreviewSize() {
        return mPreviewSize;
    }

    public void setOnPreviewReady(PreviewReadyCallback cb) {
        mPreviewReadyCallback = cb;
    }
}
