package com.iservetech.documentocr;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

/**
 * Created by Ze Khan on 08 January 2018.
 */

public class PassportOcrProcessor extends OcrProcessor {
    public static Bitmap mrzBitmap;
    static Passport passport;

    public static Passport performOcr(Bitmap bitmap) {
        passport = new Passport();
        //passport.oriImg = bitmap;
        /*mrzBitmap = cropBitmap(bitmap, Passport.mrzRect);

        Mat mrzMat = new Mat();
        Utils.bitmapToMat(mrzBitmap, mrzMat);
        Imgproc.GaussianBlur(mrzMat, mrzMat, new Size(5, 5), 0);
        Imgproc.morphologyEx(mrzMat, mrzMat, Imgproc.MORPH_BLACKHAT, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(13, 5)));
        Utils.matToBitmap(mrzMat, mrzBitmap);

        TesseractOcr.processImage(mrzBitmap, true);
        MediaStore.Images.Media.insertImage(DocumentScannerActivity.mContext.getContentResolver(), mrzBitmap.copy(Bitmap.Config.ARGB_8888, false), "mrz_image.jpg", "Image of passport MRZ");
*/
        findFace(bitmap);
        processPassport(bitmap);

        //detectRect(mrzBitmap);


        return passport;
    }


    static void processPassport(Bitmap oriImg) {
        Bitmap mrz_processed = Bitmap.createBitmap(oriImg, 0, (int) (oriImg.getHeight() * 0.68), oriImg.getWidth(), (int) (oriImg.getHeight() * 0.32));

        double scale = 1000f / mrz_processed.getWidth();
        //double scale = 1;
        mrz_processed = Bitmap.createScaledBitmap(mrz_processed, (int) (mrz_processed.getWidth() * scale), (int) (mrz_processed.getHeight() * scale), true);
        Bitmap mrzCopy = Bitmap.createScaledBitmap(mrz_processed, mrz_processed.getWidth(), mrz_processed.getHeight(), true);

        Mat mrzMat = new Mat();
        Utils.bitmapToMat(mrz_processed, mrzMat);

        Imgproc.cvtColor(mrzMat, mrzMat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.morphologyEx(mrzMat, mrzMat, Imgproc.MORPH_BLACKHAT, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15)));
        Imgproc.GaussianBlur(mrzMat, mrzMat, new Size(3, 3), 0);
        Imgproc.threshold(mrzMat, mrzMat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        //Imgproc.morphologyEx(mrzMat, mrzMat, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, 1)));

        Utils.matToBitmap(mrzMat, mrz_processed);


        //saveFiles(mrz_processed, "mrz.jpg");
        //saveFiles(mrzCopy, "mrz_ori.jpg");

        mrzMat.release();
        //mrz_processed.recycle();

        if (BuildConfig.DEBUG) {
            File imageFile = saveImage(oriImg, "document_image.jpg");
            saveImage(mrz_processed, "processed_image.jpg");
            passport.oriImgPath = imageFile.getPath();
        }
        passport.oriImg = oriImg;

        TesseractOcr.processImage(mrz_processed, DocumentScannerActivity.ocrDetectionCompleteListener);

        /*TextRecognizer textRecognizer = new TextRecognizer.Builder(DocumentScannerActivity.mContext).build();
        Frame frame = new Frame.Builder().setBitmap(mrz_processed).build();
        SparseArray<TextBlock> textBlockSparseArray = textRecognizer.detect(frame);
        List<TextBlock> textBlockList = new ArrayList<>();
        for (int i = 0; i < textBlockSparseArray.size(); i++) {
            textBlockList.add(textBlockSparseArray.valueAt(i));
        }
        Collections.sort(textBlockList, new Comparator<TextBlock>() {
            @Override
            public int compare(TextBlock textBlock, TextBlock t1) {
                return Integer.valueOf(t1.getBoundingBox().height() * t1.getBoundingBox().width()).compareTo(textBlock.getBoundingBox().height() * textBlock.getBoundingBox().width());
            }
        });

        List<String> passportLines = new ArrayList<>();
        for (TextBlock textBlock : textBlockList) {
            android.graphics.Rect rect = textBlock.getBoundingBox();
            Log.d(TAG, "gmv: " + textBlock.getValue());
            if ((float) rect.width() / rect.height() > 5 && (float) rect.width() / mrz_processed.getWidth() > 0.5) {
                for(int i=0;i<textBlock.getComponents().size();i++){
                    String line = textBlock.getComponents().get(i).getValue();
                    if(line.length() > 10){
                        passportLines.add(line);
                    }
                }
                if (passportLines.size() > 1)
                    break;
            }
        }*/

    }

    static void imagePreProcessing(Bitmap img) {
        Mat mat = new Mat();
        Utils.bitmapToMat(img, mat);

        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);

        Mat sobel = new Mat();
        Imgproc.Sobel(mat, sobel, CvType.CV_32F, 1, 0, -1, 1, 0);
        Core.convertScaleAbs(sobel, sobel);
        Core.MinMaxLocResult minMax = Core.minMaxLoc(sobel);
        if (minMax.minVal != minMax.maxVal) {
            sobel.convertTo(mat, CvType.CV_8U, 255.0 / (minMax.maxVal - minMax.minVal), -255.0 * minMax.minVal / (minMax.maxVal - minMax.minVal));
        }

        Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(13, 5)));
        Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(21, 21)));
        Imgproc.erode(mat, mat, null, new Point(-1, -1), 4);

        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(mat.clone(), contours, null, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        Collections.sort(contours, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint matOfPoint, MatOfPoint t1) {
                return Double.valueOf(Imgproc.contourArea(t1)).compareTo(Imgproc.contourArea(matOfPoint));
            }
        });

        for (MatOfPoint contour : contours) {
            Rect rect = Imgproc.boundingRect(contour);
            double ar = (float) rect.width / rect.height;
            double crWidth = (float) rect.width / mat.cols();

            if (ar > 5 && crWidth > 0.5) {

            }
        }
    }

    private static void saveFiles(Bitmap bitmap, String filename) {
        if (BuildConfig.DEBUG) {
            File mFile = new File(DocumentScannerActivity.mContext.getExternalFilesDir(null), filename);

            FileOutputStream out = null;
            try {
                out = new FileOutputStream(mFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);// bmp is your Bitmap instance
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static void findFace(Bitmap oriImg) {
        faceDetector = new FaceDetector.Builder(DocumentScannerActivity.mContext).build();
        Frame frame = new Frame.Builder().setBitmap(oriImg).build();

        SparseArray<Face> faces = faceDetector.detect(frame);
        if (faces.size() > 0) {
            List<Face> faceList = new ArrayList<>();
            for (int i = 0; i < faces.size(); i++) {
                faceList.add(faces.valueAt(i));
            }

            Collections.sort(faceList, new Comparator<Face>() {
                @Override
                public int compare(Face face, Face t1) {
                    return Float.valueOf(t1.getHeight() * t1.getWidth()).compareTo(face.getHeight() * face.getWidth());
                }
            });

            Face face = faceList.get(0);
            int height = Math.min((int) (face.getHeight() * 1.2), oriImg.getHeight() - (int) face.getPosition().y);
            int width = (int) (3f * height / 4);
            int diff_width = (width - (int) face.getWidth()) / 2;
            int x1 = face.getPosition().x - diff_width / 2 < 0 ? 0 : (int) (face.getPosition().x - diff_width / 2);
            int x2 = x1 + width > oriImg.getWidth() ? oriImg.getWidth() : x1 + width;
            width = x2 - x1;
            Bitmap faceBmp = Bitmap.createBitmap(oriImg, x1, (int) face.getPosition().y, width, height);
            //Bitmap faceBmp = Bitmap.createBitmap(oriImg, (int) face.getPosition().x, (int) face.getPosition().y, (int) face.getWidth(), (int) (face.getHeight() * 1.2));
            passport.faceImg = faceBmp;
        }
        faceDetector.release();
    }

    public static Passport formatPassportOcr(String mrzString) {
        int newline_Ind = 0;
        int validLineInd = 0;
        mrzString = mrzString.replace(" ", "");

        boolean mrzStarted = false;

        List<String> detectedLines = new ArrayList<>();
        String nameLine = "";
        String passportLine = "";


        for (int i = 0; i < mrzString.length(); i++) {
            if (mrzString.charAt(i) == '\n' || i == mrzString.length() - 1) {
                String currentLine = mrzString.substring(newline_Ind, i);
                newline_Ind = i + 1;
                if (currentLine.length() > 30) {
                    if (currentLine.charAt(0) == 'P' || (currentLine.contains("P") && currentLine.contains("<") && nameLine.equals(""))) {
                        int pInd = currentLine.indexOf("P");
                        nameLine = currentLine.substring(pInd, currentLine.length());
                    } else if (!nameLine.equals("")) {
                        passportLine = currentLine;
                        break;
                    }
                }
            }
        }

        if (nameLine.length() >= 5) {
            for (int j = 5; j < nameLine.length(); j++) {
                passport.name += nameLine.charAt(j);
                if (passport.name.length() > 3 && passport.name.substring(passport.name.length() - 3, passport.name.length()).equals("<<<")) {
                    passport.name = passport.name.substring(0, passport.name.length() - 3).replace("<<", " ").replace("<", " ");
                    break;
                } else if (j == nameLine.length() - 1) {
                    passport.name.replace("<<", " ").replace("<", " ");
                    if (passport.name.charAt(passport.name.length() - 1) == ' ')
                        passport.name = passport.name.substring(0, passport.name.length() - 1);
                    break;
                }
            }
        }

        passport.name = similarDigits2Letters(passport.name.toUpperCase());

        try {
            passport.passportNum = findValidPassportNumber(passportLine);
            /*if (passport.passportNum == "") {
                passport.passportNum = passportLine.substring(0, 9).toUpperCase();
            }*/
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }


        /*for(int i = 0; i < mrzString.length(); i++){
            if(mrzString.charAt(i) == '\n' || i == mrzString.length()-1){
                String currentLine = mrzString.substring(newline_Ind, i);
                newline_Ind = i + 1;
                if(currentLine.length() > 30){
                    if(currentLine.charAt(0) == 'P' || mrzStarted){
                        mrzStarted = true;
                    }else{
                        continue;
                    }
                    if(passport.name == null || passport.name.equals("")){
                        passport.name = "";
                        for(int j = 5; j < currentLine.length(); j++){
                            passport.name += currentLine.charAt(j);
                            if(passport.name.length() > 3 && passport.name.substring(passport.name.length()-3, passport.name.length()).equals("<<<")){
                                passport.name = passport.name.substring(0, passport.name.length() - 3).replace("<<"," ").replace("<", " ");
                                break;
                            }else if(j == currentLine.length() -1){
                                passport.name.replace("<<"," ").replace("<", " ");
                                if(passport.name.charAt(passport.name.length() -1) == ' ')
                                    passport.name = passport.name.substring(0, passport.name.length()-1);
                                break;
                            }
                        }
                    }else{
                        try{
                            passport.passportNum = currentLine.substring(0,9);
                        }catch (StringIndexOutOfBoundsException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }*/

        return passport;
    }

    private static String findValidPassportNumber(String line) {
        //List<Character> alphabets = new ArrayList<>('A',)
        Log.d(TAG, String.valueOf((int) 'A'));
        int maxCheckInd = 5;

        Log.d(TAG, "passport line: " + line);
        try {
            for (int i = 0; i < maxCheckInd; i++) {
                if (line.length() < i + 10)
                    return "";

                if (!validPassportNum(line.substring(i, i + 10)).equals("")) {
                    return line.substring(i, i + 10);
                }

                /*int sum = 0;
                int multiplier;
                for (int j = i; j < i + 9; j++) {

                    if ((j - i) % 3 == 0) {
                        multiplier = 7;
                    } else if ((j - i) % 3 == 1) {
                        multiplier = 3;
                    } else {
                        multiplier = 1;
                    }

                    if (Character.isAlphabetic(line.charAt(j))) {
                        sum += (((int) line.charAt(j) - 55) * multiplier);
                        Log.d(TAG, "passport digit: " + line.charAt(j) + ", value: " + (((int) line.charAt(j) - 55) * multiplier));
                    } else if (Character.isDigit(line.charAt(j))) {
                        sum += (Character.getNumericValue(line.charAt(j)) * multiplier);
                        Log.d(TAG, "passport digit: " + line.charAt(j) + ", value: " + (Character.getNumericValue(line.charAt(j)) * multiplier));
                    }
                }
                int checkDigit = sum % 10;
                Log.d(TAG, "passport sequence: " + line.substring(i, i + 9));
                Log.d(TAG, "passport sum is " + sum);
                Log.d(TAG, "passport check digit is " + checkDigit + ", value read: " + line.charAt(i + 9));
                if (Character.isDigit(line.charAt(i + 9)) && checkDigit == Character.getNumericValue(line.charAt(i + 9))) {
                    Log.d(TAG, "passport number validated");
                    return line.substring(i, i + 9).replace("<", "");
                }*/
            }
        } catch (Exception e) {
            Log.e(TAG, "passport number cannot be validated: " + line);
            Log.e(TAG, e.toString());
        }


        return "";
    }

    public static String validPassportNum(String string) {
        if (string.length() != 10) {
            return "";
        }

        int i = 0;
        int multiplier;
        int sum = 0;
        for (int j = i; j < i + 9; j++) {

            if ((j - i) % 3 == 0) {
                multiplier = 7;
            } else if ((j - i) % 3 == 1) {
                multiplier = 3;
            } else {
                multiplier = 1;
            }

            if (Character.isAlphabetic(string.charAt(j))) {
                sum += (((int) string.charAt(j) - 55) * multiplier);
                Log.d(TAG, "passport digit: " + string.charAt(j) + ", value: " + (((int) string.charAt(j) - 55) * multiplier));
            } else if (Character.isDigit(string.charAt(j))) {
                sum += (Character.getNumericValue(string.charAt(j)) * multiplier);
                Log.d(TAG, "passport digit: " + string.charAt(j) + ", value: " + (Character.getNumericValue(string.charAt(j)) * multiplier));
            }
        }
        int checkDigit = sum % 10;
        Log.d(TAG, "passport sequence: " + string.substring(i, i + 9));
        Log.d(TAG, "passport sum is " + sum);
        Log.d(TAG, "passport check digit is " + checkDigit + ", value read: " + string.charAt(i + 9));
        if (Character.isDigit(string.charAt(i + 9)) && checkDigit == Character.getNumericValue(string.charAt(i + 9))) {
            Log.d(TAG, "passport number validated");
            //return true;
            return string.substring(i, i + 9).replace("<", "");
        }

        return "";
    }


    private static Passport dataFormatting(String mrzString) {
        Passport passport = new Passport();
        Log.d(TAG, mrzString);


        return new Passport();
    }
}
