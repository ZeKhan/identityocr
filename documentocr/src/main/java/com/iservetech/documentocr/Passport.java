package com.iservetech.documentocr;

import android.graphics.Bitmap;

import org.opencv.core.Rect;
import org.opencv.core.Size;

/**
 * Created by Ze Khan on 08 January 2018.
 */

public class Passport {
    public String name = "", icNum, nationality, passportNum = "", gender;
    public Bitmap faceImg, oriImg;
    public String oriImgPath = "";

    private static double realHeight = 8.5, realWidth = 12.5;
    private static double factor = 100;
    public static Size passportSize = new Size(realWidth * factor, realHeight * factor);
    public static Rect mrzRect = new Rect(round((0.3 / realWidth) * passportSize.width), round((6.7 / realHeight) * passportSize.height),
            round((11.9 / realWidth) * passportSize.width), round((1.4 / realHeight) * passportSize.height));

    Passport() {
    }

    private static int round(double calculation) {
        return (int) Math.round(calculation);
    }
}
