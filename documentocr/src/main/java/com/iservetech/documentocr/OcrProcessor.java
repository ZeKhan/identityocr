package com.iservetech.documentocr;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

/**
 * Created by Ze Khan on 09-Jan-18.
 */

public class OcrProcessor {
    static TextRecognizer textRecognizer;
    static FaceDetector faceDetector;

    protected static Bitmap performBlackHat(Bitmap bitmap) {
        Mat mrzMat = new Mat();
        Utils.bitmapToMat(bitmap, mrzMat);
        Imgproc.GaussianBlur(mrzMat, mrzMat, new Size(5, 5), 0);
        Imgproc.morphologyEx(mrzMat, mrzMat, Imgproc.MORPH_BLACKHAT, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(13, 5)));
        Utils.matToBitmap(mrzMat, bitmap);

        return bitmap;
    }

    protected static File saveImage(Bitmap bitmap, String name) {
        Log.d(TAG, "saving image file: " + name);
        File mFile = new File(DocumentScannerActivity.mContext.getExternalFilesDir(null), name);

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(mFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);// bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mFile;
    }

    protected static String detectRect(Bitmap croppedBitmap) {
        if (textRecognizer == null)
            textRecognizer = new TextRecognizer.Builder(DocumentScannerActivity.mContext).build();

        Frame frame = new Frame.Builder().setBitmap(croppedBitmap).build();

        SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);

        String text = "";
        List<TextBlock> textBlockList = new ArrayList<>();
        for (int i = 0; i < textBlocks.size(); i++) {
            textBlockList.add(textBlocks.valueAt(i));
        }

        Collections.sort(textBlockList, new Comparator<TextBlock>() {
            @Override
            public int compare(TextBlock textBlock, TextBlock t1) {
                return Integer.valueOf(textBlock.getBoundingBox().top).compareTo(t1.getBoundingBox().top);
            }
        });

        for (int i = 0; i < textBlockList.size(); i++) {
            TextBlock item = textBlockList.get(i);
            List<? extends Text> texts = item.getComponents();
            for (int j = 0; j < texts.size(); j++) {
                Log.d(TAG, texts.get(j).getValue());
            }
            text += item.getValue();
            if (i != textBlockList.size() - 1)
                text += "\n";
        }
        text = text.toUpperCase();
        return text;
    }

    protected static Bitmap cropBitmap(Bitmap bitmap, Rect area) {
        Bitmap cropped = Bitmap.createBitmap(bitmap, area.x, area.y, area.width, area.height);
        return cropped;
    }

    protected static String similarLetters2Digits(String oriString) {
        return oriString.replace("I", "1").replace("L", "1").replace("S", "5").replace("Z", "2")
                .replace("O", "0").replace("B", "8").replace("G", "6");
    }

    protected static String similarDigits2Letters(String oriString) {
        return oriString.replace("1", "I").replace("5", "S").replace("2", "Z").replace("0", "O")
                .replace("8", "B").replace("6", "G");
    }

    protected static String insertIntoString(String oriString, CharSequence charSequence, int index) {
        return oriString.substring(0, index) + charSequence + oriString.substring(index, oriString.length());
    }

    protected static int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }
}
