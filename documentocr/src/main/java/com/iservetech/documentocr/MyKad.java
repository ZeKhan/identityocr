package com.iservetech.documentocr;

import android.graphics.Bitmap;

import org.opencv.core.Rect;
import org.opencv.core.Size;

/**
 * Created by Ze Khan on 04-Jan-18.
 */

public class MyKad {
    public String icNum = "", name = "", address = "", gender = "", warning;
    public Bitmap faceImg, oriImg;
    public String oriImgPath = "";
    public boolean success;
    public static double realHeight = 5.4, realWidth = 8.6;
    private static double factor = 100;
    public static Size myKadSize = new Size(realWidth * factor, realHeight * factor);
    public static Rect icNumRect = new Rect(0, round((0.9 / realHeight) * myKadSize.height), round((3.5 / realWidth) * myKadSize.width), round((0.7 / realHeight) * myKadSize.height));
    public static Rect nameRect = new Rect(0, round((3 / realHeight) * myKadSize.height), round((4.0 / realWidth) * myKadSize.width), round((1.15 / realHeight) * myKadSize.height));
    public static Rect addressRect = new Rect(0, round((4 / realHeight) * myKadSize.height), round((3.8 / realWidth) * myKadSize.width), round((1.4 / realHeight) * myKadSize.height));
    public static Rect genderRect = new Rect(round((6.7 / realWidth) * myKadSize.width), round((4.75 / realHeight) * myKadSize.height), round((1.8 / realWidth) * myKadSize.width), round((0.4 / realHeight) * myKadSize.height));

    MyKad() {
    }

    private static int round(double calculation) {
        return (int) Math.round(calculation);
    }
}
