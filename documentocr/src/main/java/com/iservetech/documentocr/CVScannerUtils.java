/*
package com.iservetech.documentocr;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

*/
/**
 * Created by Ze Khan on 04-Jan-18.
 *//*


public class CVScannerUtils {
    public static Line lineFromData(double[] lineData) {
        double rho = lineData[0];
        double theta = lineData[1];
        double cosTheta = Math.cos(theta);
        double sinTheta = Math.sin(theta);
        double x0 = cosTheta * rho;
        double y0 = sinTheta * rho;
        Point pt1 = new Point(x0 + 10000 * (-sinTheta), y0 + 10000 * cosTheta);
        Point pt2 = new Point(x0 - 10000 * (-sinTheta), y0 - 10000 * cosTheta);
        Line line = new Line(pt1, pt2);

        return line;
    }

    public static Point intersection(Line l1, Line l2) {
        // y = mx + c
        double A1 = l1.A, B1 = l1.B, C1 = l1.C, A2 = l2.A, B2 = l2.B, C2 = l2.C;

        double det = (A1 * B2) - (A2 * B1);
        if (det == 0)
            return null;

        double x = ((B2 * C1) - (B1 * C2)) / det;
        double y = ((A1 * C2) - (A2 * C1)) / det;

        Point point = new Point(x, y);
        return point;
    }

    public static Mat bitmapToMap(Bitmap bitmap){
        Mat mat = new Mat();
        Utils.bitmapToMat(bitmap, mat);

        return mat;
    }

    public static Bitmap matToBitmap(Mat mat){
        Bitmap bitmap = Bitmap.createBitmap(mat.width(), mat.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bitmap);

        return bitmap;
    }

    public static List<Mat> split(Mat mat){
        List<Mat> matList = new ArrayList<>();
        Core.split(mat, matList);
        return matList;
    }

    public static MatOfPoint hull2Points(MatOfInt hull, MatOfPoint contour) {
        */
/*List<Point[]> hullPoints = new ArrayList<>();
        Point[] points = new Point[hull.rows()];
        for(int i=0;i<hull.rows();i++){
            int index = (int)hull.get(i, 0)[0];
            points[i] = new Point(contour.get(index,0)[0], contour.get(index,0)[1]);
        }

        MatOfPoint point = new MatOfPoint();
        point.fromArray(points);*//*


        List<Integer> indexes = hull.toList();
        List<Point> points = new ArrayList<>();
        MatOfPoint point= new MatOfPoint();
        for(Integer index:indexes) {
            points.add(contour.toList().get(index));
        }
        point.fromList(points);
        return point;
    }
}
*/
