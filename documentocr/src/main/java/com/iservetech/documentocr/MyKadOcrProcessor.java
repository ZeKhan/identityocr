package com.iservetech.documentocr;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

/**
 * Created by Ze Khan on 04-Jan-18.
 */

public class MyKadOcrProcessor extends OcrProcessor {
    public static Bitmap icNumBitmap, icNameBitmap, icAddBitmap, icGenderBitmap;
    private static TextRecognizer textRecognizer;
    private static FaceDetector detector;
    static MyKad mykad;

    public static MyKad performOcr(Bitmap oriBitmap) {
        mykad = new MyKad();
        //mykad.oriImg = bitmap;
        //bitmap = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth() * 1.5), (int)(bitmap.getHeight() * 1.5), false);
        double scale = 500f / oriBitmap.getWidth();
        Bitmap bitmap = Bitmap.createScaledBitmap(oriBitmap, (int) (oriBitmap.getWidth() * scale), (int) (oriBitmap.getHeight() * scale), true);
        Bitmap processedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), false);
        //Bitmap processedBmp = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight());

        //bitmap = performBlackHat(bitmap);

        /*icNumBitmap = cropBitmap(bitmap, MyKad.icNumRect);
        icNameBitmap = cropBitmap(bitmap, MyKad.nameRect);
        icAddBitmap = cropBitmap(bitmap, MyKad.addressRect);
        icGenderBitmap = cropBitmap(bitmap, MyKad.genderRect);

        myKad.address = detectRect(icAddBitmap);
        myKad.icNum = detectRect(icNumBitmap);
        myKad.name = detectRect(icNameBitmap);
        myKad.gender = detectRect(icGenderBitmap);
        myKad = dataFormatting(myKad);*/

        /*Mat mykadMat = new Mat();
        Utils.bitmapToMat(bitmap, mykadMat);

        Imgproc.cvtColor(mykadMat, mykadMat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.morphologyEx(mykadMat, mykadMat, Imgproc.MORPH_BLACKHAT, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15)));
        Imgproc.GaussianBlur(mykadMat, mykadMat, new Size(3, 3), 0);
        Imgproc.threshold(mykadMat, mykadMat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        Imgproc.erode(mykadMat, mykadMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2, 2)));
        //Imgproc.morphologyEx(mykadMat, mykadMat, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3)));

        Utils.matToBitmap(mykadMat, processedBitmap);*/

        //processedBitmap = processImage(oriBitmap);
        processedBitmap = oriBitmap;

        findFace(bitmap);
        processIcNum(processedBitmap);
        processNameAndAddress(processedBitmap);

        /*if (BuildConfig.DEBUG) {
            File mFile = new File(DocumentScannerActivity.mContext.getExternalFilesDir(null), "mykad_ori.jpg");
            File processedFile = new File(DocumentScannerActivity.mContext.getExternalFilesDir(null), "mykad.jpg");

            FileOutputStream out = null;
            FileOutputStream out_processed = null;
            try {
                out = new FileOutputStream(mFile);
                out_processed = new FileOutputStream(processedFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                processedBitmap.compress(Bitmap.CompressFormat.PNG, 100, out_processed);// bmp is your Bitmap instance
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                    if (out_processed != null) {
                        out_processed.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/

        //mykadMat.release();
        bitmap.recycle();
        //processedBitmap.recycle();
        if (BuildConfig.DEBUG) {
            File imgFile = saveImage(oriBitmap, "document_image.jpg");
            File processedImgFile = saveImage(processedBitmap, "processed_image.jpg");
            mykad.oriImgPath = imgFile.getPath();
        }
        mykad.oriImg = oriBitmap;

        return mykad;
    }

    static Bitmap processImage(Bitmap oriImg) {
        Mat mat = new Mat();

        Utils.bitmapToMat(oriImg, mat);

        /*Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(mat, mat, new Size(3, 3), 0);
        Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_BLACKHAT, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(10, 20)));
        //Imgproc.erode(mat, mat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
        //Imgproc.medianBlur(mat, mat, 2);
        Imgproc.GaussianBlur(mat, mat, new Size(5, 5), 0);
        Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        //Imgproc.erode(mat, mat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
        //Imgproc.dilate(mat, mat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2, 2)));

        Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_OPEN, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2, 2)));*/





        /*List<Mat> channels = new ArrayList<>();
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2HSV);
        Core.split(mat, channels);
        mat = channels.get(channels.size() - 1);
        Imgproc.GaussianBlur(mat,mat, new Size(3,3),0);
        Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);*/





        List<Mat> channels = new ArrayList<>();
        CLAHE clahe = Imgproc.createCLAHE();

        //Imgproc.GaussianBlur(mat, mat, new Size(3, 3), 0);
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2HSV);
        Core.split(mat, channels);


        Mat mask = new Mat();
        double thresh = Imgproc.threshold(channels.get(2), mask, 10, 255, Imgproc.THRESH_OTSU);
        thresh = thresh * 0.9;
        Log.d(TAG, "threshold value: " + thresh);
        Core.inRange(mat, new Scalar(0, 0, 0), new Scalar(180, 255, thresh), mat);
        //Core.inRange(mat, new Scalar(100, 0, 0), new Scalar(150, 255, 255), mat);
        //Imgproc.erode(mat,mat,Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
        mask.release();
        //Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5)));



        /*Imgproc.GaussianBlur(mat, mat, new Size(11, 11), 0);
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_GRADIENT, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3)));
        Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_OTSU);*/





        Utils.matToBitmap(mat, oriImg);

        mat.release();

        return oriImg;
    }

    static void processIcNum(Bitmap oriImg) {
        long start = new Date().getTime();
        Bitmap ic = Bitmap.createBitmap(oriImg, 0, (int) (oriImg.getHeight() * 0.1), (int) (oriImg.getWidth() * 0.5), (int) (oriImg.getHeight() * 0.3));
        ic = processImage(ic);
        saveImage(ic, "icnum.jpg");
        textRecognizer = new TextRecognizer.Builder(DocumentScannerActivity.mContext).build();
        Frame frame = new Frame.Builder().setBitmap(ic).build();
        SparseArray<TextBlock> items = textRecognizer.detect(frame);
        Log.e(TAG, "detecting ic num took: " + (new Date().getTime() - start) / 1000f);
        for (int i = 0; i < items.size(); i++) {
            TextBlock item = items.valueAt(i);
            List<? extends Text> texts = item.getComponents();
            for (int j = 0; j < texts.size(); j++) {
                int digits = 0;
                for (int l = 0; l < texts.get(j).getValue().length(); l++) {
                    if (Character.isDigit(texts.get(j).getValue().charAt(l)))
                        digits++;
                }
                if (digits > texts.get(j).getValue().length() / 2)
                    mykad.icNum = similarLetters2Digits(texts.get(j).getValue().toUpperCase());
            }
        }
        textRecognizer.release();
    }

    static void findFace(Bitmap oriImg) {
        long start = new Date().getTime();
        detector = new FaceDetector.Builder(DocumentScannerActivity.mContext).build();
        Frame frame = new Frame.Builder().setBitmap(oriImg).build();

        SparseArray<Face> faces = detector.detect(frame);
        if (faces.size() > 0) {
            List<Face> faceList = new ArrayList<>();
            for (int i = 0; i < faces.size(); i++) {
                faceList.add(faces.valueAt(i));
            }

            Collections.sort(faceList, new Comparator<Face>() {
                @Override
                public int compare(Face face, Face t1) {
                    return Float.valueOf(t1.getHeight() * t1.getWidth()).compareTo(face.getHeight() * face.getWidth());
                }
            });

            Face face = faceList.get(0);
            int height = Math.min((int) (face.getHeight() * 1.2), oriImg.getHeight() - (int) face.getPosition().y);
            int width = (int) (3f * height / 4);
            int diff_width = (width - (int) face.getWidth()) / 2;
            int x1 = face.getPosition().x - diff_width / 2 < 0 ? 0 : (int) (face.getPosition().x - diff_width / 2);
            int x2 = x1 + width > oriImg.getWidth() ? oriImg.getWidth() : x1 + width;
            width = x2 - x1;
            Bitmap faceBmp = Bitmap.createBitmap(oriImg, x1, (int) face.getPosition().y, width, height);
            //Bitmap faceBmp = Bitmap.createBitmap(oriImg, (int) face.getPosition().x, (int) face.getPosition().y, (int) face.getWidth(), (int) (face.getHeight() * 1.2));
            Log.e(TAG, "face image ratio: " + faceBmp.getHeight() + "/" + faceBmp.getWidth());
            mykad.faceImg = faceBmp;
        }
        Log.e(TAG, "detecting face took: " + (new Date().getTime() - start) / 1000f);
        detector.release();
    }

    static void processNameAndAddress(Bitmap oriImg) {
        long start = new Date().getTime();
        Bitmap btmLeftBitmap = Bitmap.createBitmap(oriImg, 0, (int) (oriImg.getHeight() * 0.55), (int) (oriImg.getWidth() * 0.65), (int) (oriImg.getHeight() * 0.45));
        btmLeftBitmap = processImage(btmLeftBitmap);
        saveImage(btmLeftBitmap, "name_address.jpg");
        /*Mat mat = new Mat();
        Utils.bitmapToMat(btmLeftBitmap, mat);
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_BGR2GRAY);
        Imgproc.threshold(mat, mat, 10, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
        Utils.matToBitmap(mat, btmLeftBitmap);
        saveImage(btmLeftBitmap, "name_address.jpg");
        mat.release();*/

        Log.e(TAG, "thresholding done");

        textRecognizer = new TextRecognizer.Builder(DocumentScannerActivity.mContext).build();
        Frame frame = new Frame.Builder().setBitmap(btmLeftBitmap).build();

        SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);

        Log.e(TAG, "detecting name & address took: " + (new Date().getTime() - start) / 1000f);
        String text = "";
        List<TextBlock> textBlockList = new ArrayList<>();
        for (int i = 0; i < textBlocks.size(); i++) {
            if (textBlocks.valueAt(i).getBoundingBox().left < 0.3 * oriImg.getWidth())
                textBlockList.add(textBlocks.valueAt(i));
        }

        Collections.sort(textBlockList, new Comparator<TextBlock>() {
            @Override
            public int compare(TextBlock textBlock, TextBlock t1) {
                return Integer.valueOf(textBlock.getBoundingBox().top).compareTo(t1.getBoundingBox().top);
            }
        });

        String name = "";
        String address = "";

        List<String> combinedBoxes = new ArrayList<>();
        Log.d("tag", "ori number of text block: " + textBlockList.size());
        for (int i = 0; i < textBlockList.size(); i++) {
            TextBlock textBlock = textBlockList.get(i);
            Log.d(TAG, textBlock.getValue());
            if (i == 0) {
                List<TextBlock> buffer = new ArrayList<>();
                buffer.add(textBlock);
                combinedBoxes.add(textBlock.getValue());
            } else {
                TextBlock prevBlock = textBlockList.get(i - 1);
                //Log.d("")
                if (textBlock.getBoundingBox().top - prevBlock.getBoundingBox().bottom < 0.2 * textBlock.getBoundingBox().height()) {
                    combinedBoxes.set(combinedBoxes.size() - 1, combinedBoxes.get(combinedBoxes.size() - 1) + "\n" + textBlock.getValue());
                } else {
                    List<TextBlock> buffer = new ArrayList<>();
                    buffer.add(textBlock);
                    combinedBoxes.add(textBlock.getValue());
                }
            }
        }

        try {
            if (combinedBoxes.size() > 1) {
                Log.d("tag", "more than 1 boxes");
                // assume 1st blob to be name
                name = combinedBoxes.get(0).replace("\n", " ");
                address = combinedBoxes.get(1).replace("\n", ", ");
            } else if (combinedBoxes.size() == 1) {
                Log.d("tag", "only 1 box");
                String combinedLines = combinedBoxes.get(0);
                int indSecondLine = combinedLines.indexOf("\n", combinedLines.indexOf("\n") + 1);
                name = combinedLines.substring(0, indSecondLine);
                address = combinedLines.substring(indSecondLine + 1, combinedLines.length());
            }
        } catch (StringIndexOutOfBoundsException e) {
        }

        name = similarDigits2Letters(name.replace("\n", " ").toUpperCase());
        address = address.replace("\n", ", ").toUpperCase();

        try {
            List<Integer> newLineIndex = new ArrayList<>();
            int nlInd = 0;
            while (address.indexOf(", ", nlInd) > 0) {
                newLineIndex.add(address.indexOf(", ", nlInd));
                nlInd = address.indexOf(", ", nlInd) + 2;
            }

            if (newLineIndex.size() >= 2) {
                String oriPO = address.substring(newLineIndex.get(newLineIndex.size() - 2) + 2, newLineIndex.get(newLineIndex.size() - 2) + 7);
                String formattedPO = similarLetters2Digits(oriPO);
                Log.d(TAG, "po number: " + oriPO);
                Log.d(TAG, "formatted po: " + formattedPO);
                address = address.replace(oriPO, formattedPO);
            }

        } catch (StringIndexOutOfBoundsException e) {

        } catch (ArrayIndexOutOfBoundsException e) {

        }

        Log.d("tag", "name: " + name);
        Log.d("tag", "add: " + address);

        mykad.name = name;
        mykad.address = address;
        textRecognizer.release();
    }

    private static MyKad dataFormatting(MyKad myKad) {
        if (myKad.icNum.length() > 0) {
            myKad.icNum = myKad.icNum.replace(" ", "");
            if (myKad.icNum.charAt(6) != '-') {
                myKad.icNum = insertIntoString(myKad.icNum, "-", 6);
            }

            if (myKad.icNum.charAt(9) != '-') {
                myKad.icNum = insertIntoString(myKad.icNum, "-", 9);
            }

            myKad.icNum = similarLetters2Digits(myKad.icNum);
        }

        if (myKad.gender.length() > 0) {
            String male = "LELAKI";
            String female = "PEREMPUAN";
            if (male.contains(myKad.gender) || myKad.gender.contains(male)) {
                myKad.gender = male;
            }
            if (female.contains(myKad.gender) || myKad.gender.contains(female)) {
                myKad.gender = female;
            }
        }

        if (myKad.name.length() > 0) {
            myKad.name = similarDigits2Letters(myKad.name).replace("\n", " ");
        }

        if (myKad.address.length() > 0) {
            int jalanInd = myKad.address.indexOf("JALAN");
            char prevCharBeforeJalanInd = myKad.address.charAt(jalanInd - 1);
            if (prevCharBeforeJalanInd != ' ' && prevCharBeforeJalanInd != '\n' && Character.isDigit(prevCharBeforeJalanInd)) {
                myKad.address = insertIntoString(myKad.address, " ", jalanInd);
            }
            int postCodeStart = ordinalIndexOf(myKad.address, "\n", 2) + 1;
            int postcodeLength = 5;
            String oriPostcode = myKad.address.substring(postCodeStart, postCodeStart + postcodeLength);
            String formattedPostcode = oriPostcode;
            formattedPostcode = similarLetters2Digits(formattedPostcode);
            myKad.address.replace(oriPostcode, formattedPostcode);
        }

        Log.d(TAG, myKad.address);

        return myKad;
    }
}
