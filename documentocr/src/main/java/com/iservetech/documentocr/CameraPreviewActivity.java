package com.iservetech.documentocr;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.imgproc.Imgproc;

import static com.iservetech.documentocr.CameraPreview.mCamera;
import static com.iservetech.documentocr.DocumentScannerActivity.documentType;

/**
 * Created by Ze Khan on 01-Feb-18.
 */

public class CameraPreviewActivity extends AppCompatActivity implements View.OnClickListener {
    private CameraPreview mPreview;
    private RelativeLayout mLayout;
    static Activity mActivity;
    static View overlayView;
    static View cropView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide status-bar
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Hide title-bar, must be before setContentView
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Requires RelativeLayout.
        //mLayout = new RelativeLayout(this);
        setContentView(R.layout.camera_preview);
        mLayout = findViewById(R.id.camera_view);
        overlayView = findViewById(R.id.boundary_view);
        cropView = findViewById(R.id.center);

        ImageView pictureButton = findViewById(R.id.captureButton);
        pictureButton.setOnClickListener(this);

        TextView instructions = findViewById(R.id.ocr_instructions);
        TextView title = findViewById(R.id.ocr_title);


        ocrProgressBar = new ProgressDialog(this);

        switch (documentType) {
            case IMEI:
                pictureButton.setImageDrawable(getDrawable(R.drawable.ic_read_imei_pressed));
                title.setText("IMEI Reader");
                instructions.setText("Please center the IMEI within the boundary and capture");
                break;
            case MyKad:
                pictureButton.setImageDrawable(getDrawable(R.drawable.ic_btn_read_mykad_pressed));
                title.setText("MyKad Reader");
                instructions.setText("Please place the document within the boundary and capture");
                break;
            case Passport:
                pictureButton.setImageDrawable(getDrawable(R.drawable.ic_btn_read_passport_pressed));
                title.setText("Passport Reader");
                instructions.setText("Please place the document within the boundary and capture");
                break;
        }



        mActivity = this;

        try {
            getSupportActionBar().hide();
        } catch (Exception e) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Set the second argument by your choice.
        // Usually, 0 for back-facing camera, 1 for front-facing camera.
        // If the OS is pre-gingerbreak, this does not have any effect.
        mPreview = new CameraPreview(this, 0, CameraPreview.LayoutMode.FitToParent);
        ViewGroup.LayoutParams previewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        //RelativeLayout.LayoutParams previewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        // Un-comment below lines to specify the size.
        //previewLayoutParams.height = 500;
        //previewLayoutParams.width = 500;

        // Un-comment below line to specify the position.
        //mPreview.setCenterPosition(270, 130);

        mLayout.addView(mPreview, 0, previewLayoutParams);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
        mLayout.removeView(mPreview); // This is necessary.
        mPreview = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("tag", "camera preview destroyed");
        mActivity = null;
        cropView = null;
        overlayView = null;
        if (pictureTakenListener != null) {
            pictureTakenListener.onPictureTaken(null);
        }
        if (ocrProgressBar != null) {
            ocrProgressBar.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.captureButton) {
            //new getPictureTask().execute(mPreview.getBitmap());
            mCamera.takePicture(shutterCallback, null, pictureCallback);
        }
    }

    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {
            ocrProgressBar = ProgressDialog.show(mActivity, "Data Extraction", "Please wait while we extract information from the image", true, true, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    try {
                        pictureTakenListener.onPictureTaken(null);
                    } catch (Exception e) {
                    }
                }
            });
        }
    };

    Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] bytes, Camera camera) {
            double ratio = CameraPreview.mPictureSize.width / 1000f;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) ratio;
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
            new getPictureTask().execute(bitmap);
        }
    };

    private static PictureTakenListener pictureTakenListener;
    public static ProgressDialog ocrProgressBar;

    public static void setPictureTakenListener(PictureTakenListener listener) {
        pictureTakenListener = listener;
    }

    public interface PictureTakenListener {
        void onPictureTaken(Bitmap bitmap);
    }

    static ViewGroup.LayoutParams setOptimalCropSize(ViewGroup.LayoutParams layoutParams, double widthRatio, double heightRatio, double padding) {
        int overlayWidth = overlayView.getMeasuredWidth();
        int overlayHeight = overlayView.getMeasuredHeight();

        double widthMulti = (1 - 2 * padding) * overlayWidth / widthRatio;
        double heightMulti = (1 - 2 * padding) * overlayHeight / heightRatio;
        double multi = Math.min(widthMulti, heightMulti);

        layoutParams.height = (int) (multi * heightRatio);
        layoutParams.width = (int) (multi * widthRatio);

        Log.d("tag", "crop area size: " + layoutParams.height + "/" + layoutParams.width);

        return layoutParams;
    }

    static Bitmap cropImage(Bitmap bitmap) {
        double height_ratio = (float) cropView.getMeasuredHeight() / overlayView.getMeasuredHeight();
        double width_ratio = (float) cropView.getMeasuredWidth() / overlayView.getMeasuredWidth();

        int crop_height = (int) (height_ratio * bitmap.getHeight());
        int crop_width = (int) (width_ratio * bitmap.getWidth());
        int diff_height = bitmap.getHeight() - crop_height;
        int diff_width = bitmap.getWidth() - crop_width;

        bitmap = Bitmap.createBitmap(bitmap, diff_width / 2, diff_height / 2, bitmap.getWidth() - diff_width, bitmap.getHeight() - diff_height);

        return bitmap;
    }

    private class getPictureTask extends AsyncTask<Bitmap, Integer, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            Bitmap bitmap = cropImage(bitmaps[0]);
            Log.d("tag", "bitmap size: " + bitmap.getHeight() + "/" + bitmap.getWidth());

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (pictureTakenListener != null) {
                pictureTakenListener.onPictureTaken(bitmap);
            }
        }
    }
}
