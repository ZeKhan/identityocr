/*
package com.iservetech.documentocr;

import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.gson.Gson;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.iservetech.documentocr.CVScannerUtils.hull2Points;
import static com.iservetech.documentocr.CVScannerUtils.split;
import static com.iservetech.documentocr.DocumentScannerActivity.TAG;
import static com.iservetech.documentocr.MyKad.myKadSize;

*/
/**
 * Created by Ze Khan on 04-Jan-18.
 *//*


public class MyKadImageProcessor extends ImageProcessor {
    private static double scale = 3;

    public static MyKad processMyKad(Bitmap originalImage) {
        try {
            originalBitmap = Bitmap.createBitmap(originalImage);
            Utils.bitmapToMat(originalImage, originalMat);
            cannyBitmap = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            outlineBitmap = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            houghLinesBitmap = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            perspectiveBitmap = Bitmap.createBitmap((int) myKadSize.width, (int) myKadSize.height, Bitmap.Config.ARGB_8888);
            perspectiveMat = new Mat(myKadSize, CvType.CV_8UC1);

            //maptMethod(originalMat);

            //originalBitmap = performBlackHat(originalBitmap);
            */
/*Mat cannyMat = detectCannyEdges(originalBitmap);
            Mat outlineMat = detectDocOutline(cannyMat.clone());
            Quadrilateral srcCorners = detectHoughLines(outlineMat.clone());
            performPerspectiveCorrection(srcCorners, myKadSize);

            cannyMat.release();
            outlineMat.release();*//*

            //tesseractOcr(originalBitmap);
            //myKadDetectionPipeline(originalBitmap);

            return MyKadOcrProcessor.performOcr(perspectiveBitmap);
        } catch (IndexOutOfBoundsException e) {
            Log.e(TAG, "IndexOutOfBoundsException", e);
            return new MyKad();
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "IllegalArgumentException", e);
            return new MyKad();
        } catch (NullPointerException e) {
            Log.e(TAG, "NullPointerException", e);
            return new MyKad();
        }
    }

    private static void tesseractOcr(Bitmap bitmap){
        //TesseractOcr.processImage(bitmap, false, DocumentScannerActivity.);
    }

    private static Bitmap myKadDetectionPipeline(Bitmap bitmap) {
        List<Mat> hsvList = new ArrayList<>();
        Mat hsv = new Mat();
        Imgproc.cvtColor(originalMat, hsv, Imgproc.COLOR_BGR2HSV);
        Core.split(hsv, hsvList);
        hsv.release();

        List<ContourResult> results = new ArrayList<>();
        for (Mat channel : hsvList) {
            Log.d(TAG,"Detecting canny edges");
            channel = detectCannyEdges(channel);
            Log.d(TAG, "Detecting contour");
            ContourResult result = checkContour(channel);
            if(result != null){
                results.add(result);
            }
            channel.release();
        }

        Log.d(TAG, "analysed all channels");
        Mat outlineMat = chooseBestContour(results);
        Log.d(TAG, "best contour chosen");
        DocumentScannerActivity.Quadrilateral srcCorners = detectHoughLines(outlineMat.clone());
        perspectiveBitmap = performPerspectiveCorrection(srcCorners, myKadSize);

        outlineMat.release();
        MediaStore.Images.Media.insertImage(DocumentScannerActivity.mContext.getContentResolver(), perspectiveBitmap.copy(Bitmap.Config.ARGB_8888, false), "mrz_image.jpg", "Image of passport MRZ");

        return perspectiveBitmap;
    }

    private static Mat chooseBestContour(List<ContourResult> results){
        ContourResult bestResult = new ContourResult();

        int i =0;
        for(ContourResult result:results){
            if(i == 0){
                bestResult = result;
            }else{
                if(bestResult.distance >= result.distance){
                    bestResult = result;
                }
            }
            i++;
        }

        Mat blackMat = Mat.zeros(originalMat.rows(), originalMat.cols(), CvType.CV_8UC1);
        List<MatOfPoint> contour = new ArrayList<>();
        contour.add(bestResult.contour);
        Imgproc.drawContours(blackMat, contour, -1, new Scalar(255, 255, 255), 1);
        Utils.matToBitmap(blackMat, outlineBitmap);

        Log.d(TAG, "black mat created");

        return blackMat;
    }

    private static Mat detectCannyEdges(Mat img) {
        Imgproc.GaussianBlur(img, img, new Size(5, 5), 0);
        Imgproc.medianBlur(img, img, 5);
        CLAHE clahe = Imgproc.createCLAHE(2, new Size(3, 3));
        clahe.apply(img, img);
        double thresh = Imgproc.threshold(img, img, 0, 255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
        Imgproc.GaussianBlur(img, img, new Size(5, 5), 0);
        Imgproc.Canny(img, img, thresh * 0.33, thresh);

        Utils.matToBitmap(img, cannyBitmap);

        return img;
    }

    private static Mat detectCannyEdges(Bitmap bitmap) {
        Mat rgb = new Mat();
        Mat hsv = new Mat();
        Utils.bitmapToMat(bitmap, rgb);

        Log.d(TAG, "photo size: " + rgb.size());
        Mat edges = new Mat(rgb.size(), CvType.CV_8UC1);
        Imgproc.cvtColor(rgb, edges, Imgproc.COLOR_RGB2GRAY);
        Imgproc.cvtColor(rgb, hsv, Imgproc.COLOR_RGB2HSV);
        Mat v = split(hsv).get(2);

        */
/*Imgproc.GaussianBlur(edges, edges, new Size(3, 3), 0);
        Imgproc.medianBlur(edges, edges, 5);
        Imgproc.Canny(edges, edges, 0, 100);
        Imgproc.morphologyEx(edges, edges, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5)));*//*


        performCanny(edges);
        performCanny(v);

        Core.add(edges, v, edges);
        Imgproc.morphologyEx(edges, edges, Imgproc.MORPH_CLOSE, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3)));

        Utils.matToBitmap(edges, cannyBitmap);
        rgb.release();
        return edges;
    }

    private static void performCanny(Mat mat) {
        Imgproc.GaussianBlur(mat, mat, new Size(3, 3), 0);
        Imgproc.medianBlur(mat, mat, 11);
        Imgproc.Canny(mat, mat, 0, 100);
    }

    private static ContourResult checkContour(Mat mat) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat heirarchy = new Mat();
        Imgproc.findContours(mat, contours, heirarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        Collections.sort(contours, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint matOfPoint, MatOfPoint t1) {
                return Double.valueOf(Imgproc.contourArea(t1)).compareTo(Imgproc.contourArea(matOfPoint));
            }
        });

        if(contours.size() > 5)
            contours = contours.subList(0, 5);

        ContourResult result = new ContourResult();

        for (MatOfPoint contour : contours) {
            MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());
            MatOfInt hull = new MatOfInt();
            Imgproc.convexHull(contour, hull);
            MatOfPoint hullPoint = hull2Points(hull, contour);
            MatOfPoint2f hullPoint2f = new MatOfPoint2f(hullPoint.toArray());
            double peri = Imgproc.arcLength(hullPoint2f, true);
            MatOfPoint2f approx = new MatOfPoint2f();
            Imgproc.approxPolyDP(hullPoint2f, approx, peri * 0.02, true);
            if (approx.toArray().length == 4 && Imgproc.contourArea(contour) > (mat.rows() * mat.cols()) / 7) {
                MatOfInt4 defects = new MatOfInt4();
                Imgproc.convexityDefects(contour, hull, defects);
                double distance = 0;
                Log.d(TAG, "number of defects: " + defects.rows());
                for (int i = 0; i < defects.rows(); i++) {
                    distance += defects.get(i, 0)[3];
                }
                distance = distance / defects.rows();
                result.distance = distance;
                result.contour = hullPoint;

                return result;
            }
        }

        return null;
    }

    private static class ContourResult {
        public double distance;
        public MatOfPoint contour;
        public Mat canny;

        public ContourResult() {
        }
    }

    */
/*protected static Mat detectDocOutline(Mat cannyMat) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Mat blackMat = new Mat();
        Imgproc.findContours(cannyMat, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        // sort from biggest contour to smallest
        Collections.sort(contours, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint o1, MatOfPoint o2) {
                return Double.valueOf(Imgproc.contourArea(o2)).compareTo(Imgproc.contourArea(o1));
            }
        });

        // create empty bitmap with black background and draw document outline on it
        Bitmap blackBitmap = Bitmap.createBitmap(cannyMat.width(), cannyMat.height(), Bitmap.Config.ARGB_8888);
        Mat mat= new Mat();
        Canvas canvas = new Canvas(blackBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawRect(0F, 0F, (float) cannyMat.width(), (float) cannyMat.height(), paint);
        Utils.bitmapToMat(blackBitmap, blackMat);
        Imgproc.cvtColor(blackMat, blackMat, Imgproc.COLOR_RGB2GRAY);

        MatOfInt hull = new MatOfInt();
        boolean contourObtained = false;
        for (MatOfPoint contour : contours) {
            //Imgproc.convexHull(contour, hull);
            //contour = hull2Points(hull, contour);
            MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());

            // approximate polygon shape based on contours detected
            double peri = Imgproc.arcLength(contour2f, false);
            MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
            Imgproc.approxPolyDP(contour2f, matOfPoint2f, 0.02 * peri, true);
            //contour = new MatOfPoint(matOfPoint2f.toArray());
            // take the largest quadrilateral shape
            if (matOfPoint2f.toArray().length == 4 && Imgproc.contourArea(contour) > (cannyMat.rows() * cannyMat.cols()) / 7) {
                Imgproc.convexHull(contour, hull, false);
                List<MatOfPoint> matOfPoints = Collections.singletonList(contour);
                contourObtained = true;
                Imgproc.drawContours(blackMat, matOfPoints, -1, new Scalar(255), 1);
                break;
            }
        }

        if (contourObtained) {
            Utils.matToBitmap(blackMat, outlineBitmap);
            return blackMat;
        } else {
            Log.d(TAG, "failed to find contour");
            Utils.matToBitmap(cannyMat, outlineBitmap);
            return cannyMat;
        }
    }

    protected static Quadrilateral detectHoughLines(Mat outlineMat) {
        Mat oriMat = new Mat();
        Utils.bitmapToMat(originalBitmap, oriMat);
        Mat lines = new Mat();
        Imgproc.HoughLines(outlineMat, lines, 1, Math.PI / 180, outlineMat.width() / 10);

        List<double[]> strongLinesData = new ArrayList<>();

        // detect the 4 non-overlapping lines with the highest confidence
        for (int i = 0; i < lines.rows(); i++) {
            double[] data = lines.get(i, 0);
            double rho = data[0];
            double theta = data[1];
            double cosTheta = Math.cos(theta);
            double sinTheta = Math.sin(theta);
            double x0 = cosTheta * rho;
            double y0 = sinTheta * rho;
            Point pt1 = new Point(x0 + 10000 * (-sinTheta), y0 + 10000 * cosTheta);
            Point pt2 = new Point(x0 - 10000 * (-sinTheta), y0 - 10000 * cosTheta);

            if (strongLinesData.size() < 4) {
                boolean isStrongLine = true;
                for (double[] line : strongLinesData) {
                    double rhoThreshold = 20;
                    double thetaThreshold = Math.toRadians(30);

                    double rhoDiff = Math.abs(Math.abs(rho) - Math.abs(line[0]));
                    double thetaDiff = Math.min(Math.abs(theta - line[1]), Math.abs(Math.toRadians(180) - Math.abs(theta - line[1])));

                    if (rhoDiff < rhoThreshold && thetaDiff < thetaThreshold) {
                        isStrongLine = false;
                        break;
                    }
                }

                if (isStrongLine) {
                    strongLinesData.add(data);
                    Imgproc.line(oriMat, pt1, pt2, new Scalar(0, 255, 0), 1);
                }
            } else {
                break;
            }
        }

        // find the corner points using intersection of the 4 lines
        List<Point> src_pt = new ArrayList<>();
        for (int i = 0; i < strongLinesData.size(); i++) {
            double[] currentLineData = strongLinesData.get(i);
            Line currentLine = lineFromData(currentLineData);
            int amt = 0;
            Log.d(TAG, "theta: " + currentLineData[1] + ", rho: " + currentLineData[0]);
            for (int j = i; j < strongLinesData.size(); j++) {
                double[] otherLineData = strongLinesData.get(j);
                Line otherLine = lineFromData(otherLineData);
                //Log.d(TAG, "theta 1: " + currentLineData[1] + ", theta 2: " + otherLineData[1]);
                if (Math.abs(currentLineData[1] - otherLineData[1]) > (45 * Math.PI / 180) && Math.abs(currentLineData[1] - otherLineData[1]) < (135 * Math.PI / 180)) {
                    Point intersectionPt = intersection(currentLine, otherLine);
                    if (intersectionPt != null) {
                        amt++;
                        src_pt.add(intersectionPt);
                        Imgproc.drawMarker(oriMat, intersectionPt, new Scalar(255, 0, 0), Imgproc.MARKER_DIAMOND, 2, 2, Imgproc.LINE_4);
                    }
                }
            }
        }

        Utils.matToBitmap(oriMat, houghLinesBitmap);
        oriMat.release();
        return new Quadrilateral(src_pt);
    }

    protected static Bitmap performPerspectiveCorrection(Quadrilateral srcCorners) {
        Mat oriMat = originalMat.clone();
        List<Point> dst_pt = new ArrayList<>();
        dst_pt.add(new Point(0, 0));
        dst_pt.add(new Point(0, myKadSize.height));
        dst_pt.add(new Point(myKadSize.width, 0));
        dst_pt.add(new Point(myKadSize.width, myKadSize.height));
        Quadrilateral dstCorners = new Quadrilateral(dst_pt);

        Mat startM = Converters.vector_Point2f_to_Mat(srcCorners.sortedList);
        Mat endM = Converters.vector_Point2f_to_Mat(dstCorners.sortedList);
        Mat perspectiveTransform = Imgproc.getPerspectiveTransform(startM, endM);
        Imgproc.warpPerspective(oriMat, perspectiveMat, perspectiveTransform, myKadSize, Imgproc.INTER_CUBIC);

        Utils.matToBitmap(perspectiveMat, perspectiveBitmap);

        startM.release();
        endM.release();
        perspectiveTransform.release();
        oriMat.release();
        return perspectiveBitmap;
    }*//*


    private static Mat imageCleanup(Bitmap perspectiveBitmap) {
        Mat perspectiveMat = new Mat();
        Utils.bitmapToMat(perspectiveBitmap, perspectiveMat);
        Imgproc.cvtColor(perspectiveMat, perspectiveMat, Imgproc.COLOR_RGB2GRAY, 4);
        Mat rectKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(13, 5));
        Mat sharpenedMat = new Mat();
        Imgproc.GaussianBlur(perspectiveMat, sharpenedMat, new Size(3, 3), 0);
        Core.addWeighted(perspectiveMat, 1.5, sharpenedMat, -0.5, 0, perspectiveMat);
        //sharpenedMat.submat(MyKad.icNumRect);
        //Imgproc.morphologyEx(perspectiveMat,perspectiveMat,Imgproc.MORPH_BLACKHAT,rectKernel);
        //Imgproc.threshold(perspectiveMat,perspectiveMat,20,255,Imgproc.THRESH_BINARY);
        return sharpenedMat;
    }

    private static String performOCR(Bitmap bitmap) {
        //TesseractOcr.processImage(bitmap, false);
        Mat gray = imageCleanup(bitmap);
//        Utils.bitmapToMat(bitmap, gray);
//        Imgproc.cvtColor(gray,gray,Imgproc.COLOR_RGB2GRAY, 4);
        Utils.matToBitmap(gray, bitmap);
        DocumentScannerActivity.imageView.setImageBitmap(bitmap);
        Frame frame = new Frame.Builder().setBitmap(bitmap).build();
        TextRecognizer textRecognizer = new TextRecognizer.Builder(DocumentScannerActivity.mContext).build();

        SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);
        for (int i = 0; i < textBlocks.size(); i++) {
            TextBlock item = textBlocks.valueAt(i);
            List<? extends Text> texts = item.getComponents();
            for (int j = 0; j < texts.size(); j++) {
                Log.d(TAG, texts.get(j).getValue());
            }
            //Log.d(TAG, item.getValue());
        }

        MyKad myKad = new MyKad();
        Gson gson = new Gson();
        String myKadInfo = gson.toJson(myKad);

        return myKadInfo;
    }
}
*/
