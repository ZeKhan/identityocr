package com.iservetech.documentocr;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.vision.text.TextRecognizer;
import com.iserve.newlayout.ImeiCodePreview;
import com.iserve.newlayout.MyKadPreview;
import com.iserve.newlayout.PassportPreview;
import com.iserve.newlayout.texttracker.CameraFunction;
import com.iservetech.documentocr.camera.Camera2BasicFragment;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.iservetech.documentocr.CameraPreviewActivity.ocrProgressBar;


//import devliving.online.cvscanner.util.CVProcessor;

/**
 * Created by Ze Khan on 26-Dec-17.
 */

public class DocumentScannerActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, OcrDisclaimerDialog.DisclaimerDialogListener {
    final static String STATE_COUNTER = "counter";
    final static String TAG = "scannerActivity";
    final static String DOCUMENT = "document";
    final static int CAMERA_REQUEST = 9251;
    final static int PERMISSION_REQUEST = 1;
    private final static int REQUEST_CAMERA_PERMISSION = 2;
    public static Context mContext;
    public static Activity mActivity;
    public final static String DOCUMENT_TYPE = "documentType";
    public final static String IS_PASSPORT = "isPassport";
    public final static String IS_MYKAD = "isMykad";
    public final static String IS_IMEI = "isImei";
    static boolean isPassport;
    static Document documentType = Document.MyKad;
    public static ImageView imageView;
    private Bundle mBundle;
    static File mImageFile;
    private int mCounter;

    public static Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };

    //public static ProgressDialog ocrProgressBar;

    @Override
    public void onDialogPositiveClick(String string) {
        Log.d(TAG, "positive click");
        //startCamera();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }else{
            startCustomCamera();
        }
    }

    @Override
    public void onDialogNegativeClick(String string) {
        Log.d(TAG, "negative click");
        finish();
    }

    public enum Document {MyKad, Passport, IMEI}

    static {
        System.loadLibrary("opencv_java3");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCounter = savedInstanceState.getInt(STATE_COUNTER, 0);
        }

        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.scanner_contour);

        OcrDisclaimerDialog disclaimerDialog = new OcrDisclaimerDialog();
        disclaimerDialog.setCancelable(false);
        disclaimerDialog.show(getSupportFragmentManager(), "DisclaimerDialogFragment");


        mContext = this;
        mActivity = this;

        mBundle = savedInstanceState;

        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
        }
        //DocumentScannerActivity.ocrProgressBar = new ProgressDialog(DocumentScannerActivity.mActivity);


        /*boolean cameraPermitted = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        boolean writePermitted = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (writePermitted) {
            startCameraActivity();
        }else{
            List<String> permissions = new ArrayList<>();
//            if(!cameraPermitted)
//                permissions.add(Manifest.permission.CAMERA);

            if(!writePermitted)
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

            Log.d(TAG, permissions.toString());
            ActivityCompat.requestPermissions(this, permissions.toArray(new String[0]), PERMISSION_REQUEST);
        }*/

    }

    private void startCamera(){

        //imageView = (ImageView) findViewById(R.id.contour_view);

        if (null == mBundle) {
            /*Camera2BasicFragment fragment = Camera2BasicFragment.newInstance();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.scanner_contour, fragment);
            transaction.commit();*/

            //Camera2BasicFragment.setPictureTakenListener(mPictureTakenListener);
            Bundle bundle = new Bundle();
            bundle.putSerializable(DOCUMENT_TYPE, documentType);
            Camera2BasicFragment fragment = Camera2BasicFragment.newInstance();
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.scanner_contour, fragment)
                    .commit();
        }

        Log.d(TAG, "Starting camera");
        Intent mIntent = getIntent();
        Log.d(TAG, ((Document) mIntent.getSerializableExtra(DOCUMENT)).toString());

        TextRecognizer textRecognizer = new TextRecognizer.Builder(mContext).build();
        if(textRecognizer.isOperational()){
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                LowStorageDialog lowStorageDialog = new LowStorageDialog();
                lowStorageDialog.show(getSupportFragmentManager(), "lowStorageFragment");
            }
        }
        TesseractOcr.initialiseTesseract();
    }

    private void startCustomCamera(){
        Intent intent = new Intent();
        TextRecognizer textRecognizer = new TextRecognizer.Builder(mContext).build();
        if(!textRecognizer.isOperational()){
            Log.d(TAG, "text recognizer not working");
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LowStorageDialog lowStorageDialog = new LowStorageDialog();
                    lowStorageDialog.show(getSupportFragmentManager(), "lowStorageFragment");
                    /*String text = "Text detection library failed to download. Please ensure you have sufficient memory and is connected to the internet. This process is only required for the first time";
                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();*/
                    //currentToast.show();
                }
            });
            /*IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Log.d(TAG, "text recognizer low storage");
                LowStorageDialog lowStorageDialog = new LowStorageDialog();
                lowStorageDialog.show(getSupportFragmentManager(), "lowStorageFragment");
            }*/
        }
        TesseractOcr.initialiseTesseract();
        intent = new Intent(this, CameraPreviewActivity.class);
        CameraPreviewActivity.setPictureTakenListener(mPictureTakenListener);
        startActivity(intent);

        /*switch (documentType){
            case Passport:
                intent = new Intent(this, PassportPreview.class);
                break;
            case MyKad:
                intent = new Intent(this, MyKadPreview.class);
                break;
            case IMEI:
                intent = new Intent(this, ImeiCodePreview.class);
                break;
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        CameraFunction.setPictureTakenListener(pictureTakenListener);
        startActivity(intent);*/
    }

    private void startCameraActivity() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
            Log.d(TAG, getApplicationContext().getPackageName());
            Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".fileprovider", photoFile);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            terminateDocumentScanner();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        storageDir = new File(Environment.getExternalStorageDirectory(), "Yakin");
        if (!storageDir.exists())
            storageDir.mkdir();

        mImageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        return mImageFile;
    }

    public static void readMyKad(Activity mActivity, DocumentScannerListener listener) {
        Intent scannerIntent = new Intent(mActivity, DocumentScannerActivity.class);
        scannerIntent.putExtra(DOCUMENT, Document.MyKad);
        documentType = Document.MyKad;
        mListener = listener;
        isPassport = false;
        mActivity.startActivity(scannerIntent);
    }

    public static void readPassport(Activity mActivity, DocumentScannerListener listener) {
        Intent scannerIntent = new Intent(mActivity, DocumentScannerActivity.class);
        scannerIntent.putExtra(DOCUMENT, Document.Passport);
        documentType = Document.Passport;
        mListener = listener;
        isPassport = true;
        mActivity.startActivity(scannerIntent);
    }

    public static void readIMEI(Activity mActivity, DocumentScannerListener listener){
        Intent scannerIntent = new Intent(mActivity, DocumentScannerActivity.class);
        scannerIntent.putExtra(DOCUMENT, Document.IMEI);
        documentType = Document.IMEI;
        mListener = listener;
        isPassport = true;
        mActivity.startActivity(scannerIntent);
    }

    private static void startActivity(Activity activity){
        Intent scannerIntent = new Intent(activity, DocumentScannerActivity.class);
        activity.startActivity(scannerIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST) {
            boolean permissionsGranted = true;
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    permissionsGranted = false;
                    break;
                }
            }

            if (permissionsGranted) {
                startCameraActivity();
            } else {
                terminateDocumentScanner();
            }
        }else if(requestCode == REQUEST_CAMERA_PERMISSION){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                startCustomCamera();
            }else{
                terminateDocumentScanner();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, String.valueOf(resultCode));
        if (resultCode == RESULT_OK) {
            Log.d(TAG, "Results obtained");
            try {
                Bitmap photo = setPhoto();

                if (photo == null) {
                    terminateDocumentScanner();
                    return;
                }

                imageView.setImageBitmap(photo);

                //detectEdges(photo);
                switch (documentType) {
                    case MyKad:
                        //mListener.onMyKadOcrCompleted(MyKadImageProcessor.processMyKad(photo));
                        //imageView.setImageBitmap(MyKadImageProcessor.perspectiveBitmap);
                        break;
                    case Passport:
                        //mListener.onPassportOcrCompleted(PassportImageProcessor.processPassport(photo));
                        break;
                }

                Log.d(TAG, "result obtained successfully");
                finish();
            } catch (NullPointerException e) {
                Log.e(TAG, e.toString());
                terminateDocumentScanner();
            }
        } else {
            terminateDocumentScanner();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(ocrProgressBar != null){
            try {
                CameraFunction.activeContext.finish();
                ocrProgressBar.dismiss();
            }catch (Exception e){}
        }
    }

    public static void startProgressDialog(String title, String message) {

    }

    CameraPreviewActivity.PictureTakenListener mPictureTakenListener = new CameraPreviewActivity.PictureTakenListener() {
        @Override
        public void onPictureTaken(Bitmap bitmap) {
            //ocrProgressBar.setMessage("Reading Information");
            if(bitmap == null) {
                finish();
                return;
            }

            switch (documentType){
                case Passport:
                    PassportOcrProcessor.performOcr(bitmap);
                    break;
                case MyKad:
                    mListener.onMyKadOcrCompleted(MyKadOcrProcessor.performOcr(bitmap));
                    ocrProgressBar.dismiss();
                    finish();
                    break;
                case IMEI:
                    mListener.onImeiOcrCompleted(ImeiOcrProcessor.performOcr(bitmap));
                    ocrProgressBar.dismiss();
                    finish();
                    break;
            }
        }
    };

    CameraFunction.PictureTakenListener pictureTakenListener = new CameraFunction.PictureTakenListener() {
        @Override
        public void onPictureTaken(Bitmap bitmap) {
            //ocrProgressBar.setMessage("Reading Information");
            try {
                if (bitmap == null) {
                    CameraPreviewActivity.mActivity.finish();
                    finish();
                    return;
                }

                switch (documentType) {
                    case Passport:
                        PassportOcrProcessor.performOcr(bitmap);
                        break;
                    case MyKad:
                        ocrProgressBar.dismiss();
                        CameraPreviewActivity.mActivity.finish();
                        mListener.onMyKadOcrCompleted(MyKadOcrProcessor.performOcr(bitmap));
                        finish();
                        break;
                    case IMEI:
                        ocrProgressBar.dismiss();
                        CameraPreviewActivity.mActivity.finish();
                        mListener.onImeiOcrCompleted(ImeiOcrProcessor.performOcr(bitmap));
                        finish();
                        break;
                }
            }catch (Exception e){}
        }
    };

    public static TesseractOcr.OcrDetectionCompleteListener ocrDetectionCompleteListener = new TesseractOcr.OcrDetectionCompleteListener() {
        @Override
        public void onOcrDetectionComplete(String text) {
            try {
                if (text == null) {
                    text = "";
                }
                switch (documentType) {
                    case Passport:
                        ocrProgressBar.dismiss();
                        CameraPreviewActivity.mActivity.finish();
                        mListener.onPassportOcrCompleted(PassportOcrProcessor.formatPassportOcr(text));
                        mActivity.finish();
                        break;
                    case MyKad:
                        break;
                    case IMEI:
                        break;
                }
            }catch (Exception e){}
        }
    };

    private Bitmap setPhoto() {
        View layoutView = findViewById(R.id.scanner_contour);

        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        Matrix matrix = new Matrix();
        try {
            ExifInterface exifInterface = new ExifInterface(mImageFile.getPath());
            Log.d(TAG, "orientation: " + exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION));
            switch (Integer.valueOf(exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION))) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
            }
        } catch (IOException e) {
        }

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mImageFile.getAbsolutePath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        //int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        int scaleFactor = 3;

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mImageFile.getAbsolutePath(), bmOptions);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        mImageFile.delete();

        return bitmap;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_COUNTER, mCounter);
    }

    private void detectEdges(Bitmap bitmap) {
        //cvscannerMethod(bitmap);

        Mat rgba = new Mat();
        Utils.bitmapToMat(bitmap, rgba);

        Log.d(TAG, "photo size: " + rgba.size());

        Mat edges = new Mat(rgba.size(), CvType.CV_8UC1);
        Imgproc.cvtColor(rgba, edges, Imgproc.COLOR_RGB2GRAY, 4);
        Imgproc.GaussianBlur(edges, edges, new Size(5, 5), 2);
        Imgproc.Canny(edges, edges, 50, 200);
        //Imgproc.floodFill(edges, edges, new Point(0, 0), new Scalar(1));

        Utils.matToBitmap(edges, bitmap);
        imageView.setImageBitmap(bitmap);

        Log.d(TAG, "imported mat size: " + edges.size());

        convexHullMethod(bitmap, rgba, edges);
    }

    private void houghLinePMethod(Bitmap bitmap, Mat rgba, Mat edges) {
        Mat lines = new Mat();
        Imgproc.HoughLinesP(edges, lines, 1, Math.PI / 180, bitmap.getWidth() / 5, bitmap.getWidth() / 5, bitmap.getWidth() / 10);

        Log.d(TAG, lines.rows() + " lines detected");
        Log.d(TAG, "line mat size; " + lines.size().toString());
        for (int x = 0; x < lines.rows(); x++) {
            double[] vec = lines.get(x, 0);
            double x1 = vec[0],
                    y1 = vec[1],
                    x2 = vec[2],
                    y2 = vec[3];

            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);

            Imgproc.line(rgba, start, end, new Scalar(0, 255, 0), 1);

            Line l2 = new Line(start, end);

            Log.d(TAG, "Line equation " + x + ": " + l2.A + "x + " + l2.B + "y = " + l2.C);
            for (int i = x + 1; i < lines.rows(); i++) {
                double[] prevVec;
                prevVec = lines.get(i, 0);
                Point prevStart = new Point(prevVec[0], prevVec[1]);
                Point prevEnd = new Point(prevVec[2], prevVec[3]);

                Line l1 = new Line(prevStart, prevEnd);

                Point intersectionPt = intersection(l1, l2);
                if (intersectionPt != null) {
                    Log.d(TAG, "Intersection found: " + intersectionPt.toString());
                    double angle = intersectionAngle(start, prevStart, intersectionPt, l1, l2);
                    Log.d(TAG, "Intersection angle: " + angle);
                    Imgproc.drawMarker(rgba, intersectionPt, new Scalar(255, 0, 0), Imgproc.MARKER_DIAMOND, 2, 2, Imgproc.LINE_4);
                }
            }
        }

        Utils.matToBitmap(rgba, bitmap);
        imageView.setImageBitmap(bitmap);
    }

    private void houghLineMethod(Bitmap bitmap, Mat rgba, Mat edges) {
        Mat lines = new Mat();
        Imgproc.HoughLines(edges, lines, 1, Math.PI / 180, bitmap.getWidth() / 5);

        Log.d(TAG, "line mat size; " + lines.size().toString());
        List<double[]> strongLinesData = new ArrayList<>();
        //List<>
        for (int i = 0; i < lines.rows(); i++) {
            double[] data = lines.get(i, 0);
            double rho = data[0];
            double theta = data[1];
            double cosTheta = Math.cos(theta);
            double sinTheta = Math.sin(theta);
            double x0 = cosTheta * rho;
            double y0 = sinTheta * rho;
            Point pt1 = new Point(x0 + 10000 * (-sinTheta), y0 + 10000 * cosTheta);
            Point pt2 = new Point(x0 - 10000 * (-sinTheta), y0 - 10000 * cosTheta);

            Log.d(TAG, "Rho: " + rho + " ,Theta: " + Math.toDegrees(theta));
            if (strongLinesData.size() < 4) {
                boolean isStrongLine = true;
                for (double[] line : strongLinesData) {
                    double rhoThreshold = 10;
                    double thetaThreshold = Math.PI / 18;
                    double rhoDiff = Math.abs(rho - line[0]);
                    double thetaDiff = Math.abs(theta - line[1]);

                    if (rhoDiff < rhoThreshold && thetaDiff < thetaThreshold) {
                        isStrongLine = false;
                        break;
                    }
                }

                if (isStrongLine) {
                    strongLinesData.add(data);
                    Imgproc.line(rgba, pt1, pt2, new Scalar(0, 255, 0), 1);
                }
            } else {
                break;
            }
        }

        Log.d(TAG, "number of lines: " + strongLinesData.size());
        //Log.d(TAG, "")
        List<Point> src_pt = new ArrayList<>();
        for (int i = 0; i < strongLinesData.size(); i++) {
            double[] currentLineData = strongLinesData.get(i);
            Line currentLine = lineFromData(currentLineData);
            int amt = 0;
            for (int j = i; j < strongLinesData.size(); j++) {
                double[] otherLineData = strongLinesData.get(j);
                Line otherLine = lineFromData(otherLineData);
                Log.d(TAG, "theta 1: " + currentLineData[1] + ", theta 2: " + otherLineData[1]);
                if (Math.abs(currentLineData[1] - otherLineData[1]) > (70 * Math.PI / 180) && Math.abs(currentLineData[1] - otherLineData[1]) < (110 * Math.PI / 180)) {
                    Point intersectionPt = intersection(currentLine, otherLine);
                    if (intersectionPt != null) {
                        amt++;
                        src_pt.add(intersectionPt);
                        Imgproc.drawMarker(rgba, intersectionPt, new Scalar(255, 0, 0), Imgproc.MARKER_DIAMOND, 2, 2, Imgproc.LINE_4);
                    }
                }
            }
            Log.d(TAG, "line " + i + ": " + amt + " intersections");
        }

        Log.d(TAG, "number of pts: " + src_pt.size());
        Quadrilateral srcCorners = new Quadrilateral(src_pt);

        List<Point> dst_pt = new ArrayList<>();
        dst_pt.add(new Point(0, 0));
        dst_pt.add(new Point(0, 270));
        dst_pt.add(new Point(430, 0));
        dst_pt.add(new Point(430, 270));
        Quadrilateral dstCorners = new Quadrilateral(dst_pt);

        Log.d(TAG, "sorted order: " + srcCorners.sortedList.toString());
        Log.d(TAG, "sorted order: " + dstCorners.sortedList.toString());

        Mat startM = Converters.vector_Point2f_to_Mat(srcCorners.sortedList);
        Mat endM = Converters.vector_Point2f_to_Mat(dstCorners.sortedList);
        Mat perspectiveTransform = Imgproc.getPerspectiveTransform(startM, endM);
        Mat outputMat = new Mat(new Size(430, 270), CvType.CV_8UC1);
        Imgproc.warpPerspective(rgba, outputMat, perspectiveTransform, new Size(430, 270), Imgproc.INTER_CUBIC);

        Bitmap outputBitmap = Bitmap.createBitmap(430, 270, Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(outputMat, outputBitmap);
        Utils.matToBitmap(rgba, bitmap);
        imageView.setImageBitmap(outputBitmap);
    }

    private Line lineFromData(double[] lineData) {
        double rho = lineData[0];
        double theta = lineData[1];
        double cosTheta = Math.cos(theta);
        double sinTheta = Math.sin(theta);
        double x0 = cosTheta * rho;
        double y0 = sinTheta * rho;
        Point pt1 = new Point(x0 + 10000 * (-sinTheta), y0 + 10000 * cosTheta);
        Point pt2 = new Point(x0 - 10000 * (-sinTheta), y0 - 10000 * cosTheta);
        Line line = new Line(pt1, pt2);

        return line;
    }

    private void convexHullMethod(Bitmap bitmap, Mat rgba, Mat edges) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Mat blackMat = new Mat();
        Imgproc.findContours(edges, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        Collections.sort(contours, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint o1, MatOfPoint o2) {
                return Double.valueOf(Imgproc.contourArea(o2)).compareTo(Imgproc.contourArea(o1));
            }
        });

        MatOfInt hull = new MatOfInt();
        for (MatOfPoint contour : contours) {
            MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());

            double peri = Imgproc.arcLength(contour2f, true);
            MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
            Imgproc.approxPolyDP(contour2f, matOfPoint2f, 0.02 * peri, true);

            if (matOfPoint2f.toArray().length == 4) {
                Log.d(TAG, "Document detected");
                Imgproc.convexHull(contour, hull);
                List<MatOfPoint> matOfPoints = Collections.singletonList(contour);
                Bitmap blackBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(blackBitmap);
                Paint paint = new Paint();
                paint.setColor(Color.BLACK);
                canvas.drawRect(0F, 0F, (float) bitmap.getWidth(), (float) bitmap.getHeight(), paint);

                Utils.bitmapToMat(blackBitmap, blackMat);
                Imgproc.cvtColor(blackMat, blackMat, Imgproc.COLOR_RGB2GRAY);
                Imgproc.drawContours(blackMat, matOfPoints, -1, new Scalar(255), 1);
                //Imgproc.threshold(edges,edges,0,255,Imgproc.THRESH_BINARY_INV);
                break;
            }
        }

        //Utils.matToBitmap(blackMat, bitmap);
        //imageView.setImageBitmap(bitmap);

        houghLineMethod(bitmap, rgba, blackMat);
    }

    private Point intersection(Line l1, Line l2) {
        // y = mx + c
        double A1 = l1.A, B1 = l1.B, C1 = l1.C, A2 = l2.A, B2 = l2.B, C2 = l2.C;

        double det = (A1 * B2) - (A2 * B1);
        if (det == 0)
            return null;

        double x = ((B2 * C1) - (B1 * C2)) / det;
        double y = ((A1 * C2) - (A2 * C1)) / det;

        Point point = new Point(x, y);
        return point;
    }

    private double intersectionAngle(Point p1, Point p2, Point c, Line l1, Line l2) {
        double angle1 = Math.atan2(Math.toRadians(l1.y1 - l1.y2), Math.toRadians(l1.x1 - l1.x2));
        double angle2 = Math.atan2(Math.toRadians(l2.y1 - l2.y2), Math.toRadians(l2.x1 - l2.x2));
        return Math.abs(angle1 - angle2);
        /*double x1 = p1.x, y1 = p1.y, x2 = p2.x, y2 = p2.y, x = c.x, y = c.y;

        double dist1 = Math.sqrt(Math.pow((x1 - x), 2) + Math.pow((y1 - y), 2));
        double dist2 = Math.sqrt(Math.pow((x2 - x), 2) + Math.pow((y2 - y), 2));

        double Ax, Ay;
        double Bx, By;
        double Cx, Cy;

        Cx = x;
        Cy = y;
        if (dist1 < dist2) {
            Bx = x1;
            By = y1;
            Ax = x2;
            Ay = y2;


        } else {
            Bx = x2;
            By = y2;
            Ax = x1;
            Ay = y1;
        }


        double Q1 = Cx - Ax;
        double Q2 = Cy - Ay;
        double P1 = Bx - Ax;
        double P2 = By - Ay;

        double angle = Math.acos(Math.toRadians((P1 * Q1 + P2 * Q2) / (Math.sqrt(P1 * P1 + P2 * P2) * Math.sqrt(Q1 * Q1 + Q2 * Q2))));

        angle = angle * 180 / Math.PI;*/

        //return angle;
    }

    private void minAreaMethod(Bitmap bitmap, Mat rgba, Mat edges) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(edges, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        for (MatOfPoint contour : contours) {
            RotatedRect rotatedRect = Imgproc.minAreaRect(new MatOfPoint2f(contour.toArray()));
            Imgproc.boxPoints(rotatedRect, edges);

            Point[] cornerPoints = new Point[4];
            rotatedRect.points(cornerPoints);
            Rect rect = Imgproc.boundingRect(contour);

            for (int i = 1; i < cornerPoints.length; i++) {
                Point currentPt = cornerPoints[i];
                Point prevPt = cornerPoints[i - 1];
                Imgproc.line(rgba, prevPt, currentPt, new Scalar(0, 255, 0));
            }
            Imgproc.line(rgba, cornerPoints[cornerPoints.length - 1], cornerPoints[0], new Scalar(0, 255, 0));
            //Imgproc.rectangle(rgba, rect.tl(), rect.br(), new Scalar(255, 255, 255), 1);
        }

        Utils.matToBitmap(rgba, bitmap);
        imageView.setImageBitmap(bitmap);
    }

    private void approxPolyMethod(Bitmap bitmap, Mat rgba, Mat edges) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(edges, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        for (MatOfPoint contour : contours) {
            MatOfPoint2f approxyPoly = new MatOfPoint2f();
            Imgproc.approxPolyDP(new MatOfPoint2f(contour.toArray()), approxyPoly, 0.01 * Imgproc.arcLength(new MatOfPoint2f(contour.toArray()), true), true);

            if (approxyPoly.toArray().length == 4) {
                List<MatOfPoint> matOfPointList = new ArrayList<>();
                matOfPointList.add(new MatOfPoint(approxyPoly.toArray()));
                Imgproc.drawContours(rgba, matOfPointList, -1, new Scalar(0, 255, 0));
            }
        }

        Utils.matToBitmap(rgba, bitmap);
        imageView.setImageBitmap(bitmap);
    }

    private void boundingRectMethod(Bitmap bitmap, Mat rgba, Mat edges) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(edges, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        for (MatOfPoint contour : contours) {
            Rect rect = Imgproc.boundingRect(contour);
            Imgproc.rectangle(rgba, rect.tl(), rect.br(), new Scalar(255, 255, 255), 1);
        }

        Utils.matToBitmap(rgba, bitmap);
        imageView.setImageBitmap(bitmap);
    }

    private void contourMethod(Bitmap bitmap, Mat rgba, Mat edges) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(edges, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        Collections.sort(contours, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint o1, MatOfPoint o2) {
                return Double.valueOf(Imgproc.contourArea(o2)).compareTo(Imgproc.contourArea(o1));
            }
        });

        Log.d(TAG, String.valueOf(contours.size()));
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        for (int i = 0; i < contours.size(); i++) {
            MatOfPoint contour = contours.get(i);
            MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());

            double peri = Imgproc.arcLength(contour2f, true);
            MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
            Imgproc.approxPolyDP(contour2f, matOfPoint2f, 0.02 * peri, true);

            if (matOfPoint2f.toArray().length == 4) {
                Log.d(TAG, "Document detected");
                matOfPoints.add(new MatOfPoint(matOfPoint2f.toArray()));
                break;
            }
        }

        drawContour(bitmap, matOfPoints);
    }

    private void android_opencv_scan_doc_method(Bitmap bitmap, Mat oriMat, Mat edges) {
        Mat lines = new Mat();

        Imgproc.HoughLinesP(edges, lines, 1, Math.PI / 180, bitmap.getWidth() / 12, bitmap.getWidth() / 12, 20);
        Log.d(TAG, "line size: " + lines.size());
        Bitmap tempBitmap = Bitmap.createBitmap(lines.width(), lines.height(), Bitmap.Config.RGB_565);
        Utils.matToBitmap(lines, tempBitmap);
        imageView.setImageBitmap(tempBitmap);
        int w_proc = bitmap.getWidth();
        int h_proc = bitmap.getHeight();

        List<Line> horizontals = new ArrayList<>();
        List<Line> verticals = new ArrayList<>();
        for (int x = 0; x < edges.rows(); x++) {
            double[] vec = edges.get(x, 0);
            double x1 = vec[0], y1 = vec[1], x2 = vec[2], y2 = vec[3];
            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);
            Line line = new Line(start, end);
            if (Math.abs(x1 - x2) > Math.abs(y1 - y2)) {
                horizontals.add(line);
            } else {
                verticals.add(line);
            }
        }

        if (edges.rows() > 400) {
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, "Please use a cleaner background", duration);
            toast.show();
        }

        // if we don't have at least 2 horizontal lines or vertical lines
        if (horizontals.size() < 2) {
            if (horizontals.size() == 0 || horizontals.get(0)._center.y > h_proc / 2) {
                horizontals.add(new Line(new Point(0, 0), new Point(w_proc - 1, 0)));
            }
            if (horizontals.size() == 0 || horizontals.get(0)._center.y <= h_proc / 2) {
                horizontals.add(new Line(new Point(0, h_proc - 1), new Point(w_proc - 1, h_proc - 1)));
            }
        }
        if (verticals.size() < 2) {
            if (verticals.size() == 0 || verticals.get(0)._center.x > w_proc / 2) {
                verticals.add(new Line(new Point(0, 0), new Point(h_proc - 1, 0)));
            }
            if (verticals.size() == 0 || verticals.get(0)._center.x <= w_proc / 2) {
                verticals.add(new Line(new Point(w_proc - 1, 0), new Point(w_proc - 1, h_proc - 1)));
            }
        }

        Collections.sort(horizontals, new Comparator<Line>() {
            @Override
            public int compare(Line lhs, Line rhs) {
                return (int) (lhs._center.y - rhs._center.y);
            }
        });

        Collections.sort(verticals, new Comparator<Line>() {
            @Override
            public int compare(Line lhs, Line rhs) {
                return (int) (lhs._center.x - rhs._center.x);
            }
        });

        List<Point> intersections = new ArrayList<>();

        intersections.add(computeIntersection(horizontals.get(0), verticals.get(0)));
        intersections.add(computeIntersection(horizontals.get(0), verticals.get(verticals.size() - 1)));
        intersections.add(computeIntersection(horizontals.get(horizontals.size() - 1), verticals.get(0)));
        intersections.add(computeIntersection(horizontals.get(horizontals.size() - 1), verticals.get(verticals.size() - 1)));

        for (Point point : intersections) {
            // for visualization in debug mode
            if (BuildConfig.DEBUG) {
//                Imgproc.circle(rgbMat, point, 10, new Scalar(255,255,0),3);
            }
//            point.x *= scale;
//            point.y *= scale;
        }

        double w1 = Math.sqrt(Math.pow(intersections.get(3).x - intersections.get(2).x, 2) + Math.pow(intersections.get(3).x - intersections.get(2).x, 2));
        double w2 = Math.sqrt(Math.pow(intersections.get(1).x - intersections.get(0).x, 2) + Math.pow(intersections.get(1).x - intersections.get(0).x, 2));
        double h1 = Math.sqrt(Math.pow(intersections.get(1).y - intersections.get(3).y, 2) + Math.pow(intersections.get(1).y - intersections.get(3).y, 2));
        double h2 = Math.sqrt(Math.pow(intersections.get(0).y - intersections.get(2).y, 2) + Math.pow(intersections.get(0).y - intersections.get(2).y, 2));

        double maxWidth = (w1 < w2) ? w1 : w2;
        double maxHeight = (h1 < h2) ? h1 : h2;

        // source Mat from earlier intersection calculations
        Mat srcMat = new Mat(4, 1, CvType.CV_32FC2);
        srcMat.put(0, 0, intersections.get(0).x, intersections.get(0).y, intersections.get(1).x, intersections.get(1).y, intersections.get(2).x, intersections.get(2).y, intersections.get(3).x, intersections.get(3).y);

        Mat dstMat = new Mat(4, 1, CvType.CV_32FC2);
        dstMat.put(0, 0, 0.0, 0.0, maxWidth - 1, 0.0, 0.0, maxHeight - 1, maxWidth - 1, maxHeight - 1);

        // transformation matrix
        Mat transformMatrix = Imgproc.getPerspectiveTransform(srcMat, dstMat);

        Mat finalMat = Mat.zeros((int) maxHeight, (int) maxWidth, CvType.CV_32FC2);
        Imgproc.warpPerspective(oriMat, finalMat, transformMatrix, finalMat.size());
        Bitmap finalBitmat = Bitmap.createBitmap(finalMat.width(), finalMat.height(), Bitmap.Config.RGB_565);
        Utils.matToBitmap(finalMat, finalBitmat);
        //imageView.setImageBitmap(finalBitmat);
    }

    private void cvscannerMethod(Bitmap bitmap) {
        /*Size imageSize = new Size(bitmap.getWidth(), bitmap.getHeight());
        Mat src = new Mat();
        Utils.bitmapToMat(bitmap, src);
        List<MatOfPoint> contours = devliving.online.cvscanner.util.CVProcessor.findContours(src);
        //src.release();

        //Mat finalImg = new Mat();

        //Imgproc.drawContours(src, contours, -1, new Scalar(0, 255, 0));

        if (!contours.isEmpty()) {
            CVProcessor.Quadrilateral quad = CVProcessor.getQuadrilateral(contours, imageSize);


            for (Point point : quad.points) {
                Imgproc.drawMarker(src, point, new Scalar(0, 255, 0));
            }
            if (quad != null) {
                //quad.points = CVProcessor.getUpscaledPoints(quad.points, CVProcessor.getScaleRatio(imageSize));
            }
        }
        Utils.matToBitmap(src, bitmap);
        imageView.setImageBitmap(bitmap);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);*/
    }

    private void drawContour(Bitmap bitmap, List<MatOfPoint> matOfPoints) {
        Bitmap mBitmap = bitmap;
        Mat mat = new Mat();
        Utils.bitmapToMat(mBitmap, mat);

        //Imgproc.drawContours(mat, matOfPoints, -1, new Scalar(255, 255, 255), -1);
        Utils.matToBitmap(mat, mBitmap);
        imageView.setImageBitmap(mBitmap);
    }

    public void terminateDocumentScanner() {
        Log.d(TAG, "Document Scanner Activity ended");
        if (isPassport)
            mListener.onPassportOcrCompleted(new Passport());
        else
            mListener.onMyKadOcrCompleted(new MyKad());
        if(ocrProgressBar != null){
            ocrProgressBar.dismiss();
        }
        finish();
    }

    public interface DocumentScannerListener {
        void onMyKadOcrCompleted(MyKad myKad);

        void onPassportOcrCompleted(Passport passport);

        void onImeiOcrCompleted(String imei);
    }

    protected Point computeIntersection(Line l1, Line l2) {
        double x1 = l1._p1.x, x2 = l1._p2.x, y1 = l1._p1.y, y2 = l1._p2.y;
        double x3 = l2._p1.x, x4 = l2._p2.x, y3 = l2._p1.y, y4 = l2._p2.y;
        double d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
        Point pt = new Point();
        pt.x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
        pt.y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;
        return pt;
    }

    static DocumentScannerListener mListener = null;


    class Line {
        Point _p1;
        Point _p2;
        Point _center;
        double x1, y1, x2, y2, A, B, C;

        Line(Point p1, Point p2) {
            _p1 = p1;
            _p2 = p2;
            _center = new Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
            x1 = p1.x;
            y1 = p1.y;
            x2 = p2.x;
            y2 = p2.y;

            A = y2 - y1;
            B = x1 - x2;
            C = (A * x1) + (B * y1);
        }
    }

    class Quadrilateral {
        Point tl, tr, bl, br;
        List<Point> sortedList;

        Quadrilateral(List<Point> pointList) {
            if (pointList.size() != 4)
                return;

            Collections.sort(pointList, new Comparator<Point>() {
                @Override
                public int compare(Point p1, Point p2) {
                    return Double.compare(p1.x, p2.x);
                }
            });

            List<Point> leftMost = pointList.subList(0, 2);
            List<Point> rightMost = pointList.subList(2, 4);

            Collections.sort(leftMost, new Comparator<Point>() {
                @Override
                public int compare(Point point, Point t1) {
                    return Double.compare(point.y, t1.y);
                }
            });

            Collections.sort(rightMost, new Comparator<Point>() {
                @Override
                public int compare(Point point, Point t1) {
                    return Double.compare(point.y, t1.y);
                }
            });

            tl = leftMost.get(0);
            bl = leftMost.get(1);
            tr = rightMost.get(0);
            br = rightMost.get(1);
            sortedList = Arrays.asList(tl, tr, bl, br);
        }
    }
}
