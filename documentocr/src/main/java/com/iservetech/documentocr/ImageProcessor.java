/*
package com.iservetech.documentocr;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.iservetech.documentocr.CVScannerUtils.intersection;
import static com.iservetech.documentocr.CVScannerUtils.lineFromData;
import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

*/
/**
 * Created by Ze Khan on 09-Jan-18.
 *//*


public class ImageProcessor {
    public static Bitmap originalBitmap, cannyBitmap, outlineBitmap, houghLinesBitmap, perspectiveBitmap;
    public static Mat originalMat, perspectiveMat;

    protected static Mat detectDocOutline(Mat cannyMat) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Mat blackMat = new Mat();
        Imgproc.findContours(cannyMat, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        // sort from biggest contour to smallest
        Collections.sort(contours, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint o1, MatOfPoint o2) {
                return Double.valueOf(Imgproc.contourArea(o2)).compareTo(Imgproc.contourArea(o1));
            }
        });

        // create empty bitmap with black background and draw document outline on it
        Bitmap blackBitmap = Bitmap.createBitmap(cannyMat.width(), cannyMat.height(), Bitmap.Config.ARGB_8888);
        Mat mat = new Mat();
        Canvas canvas = new Canvas(blackBitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawRect(0F, 0F, (float) cannyMat.width(), (float) cannyMat.height(), paint);
        Utils.bitmapToMat(blackBitmap, blackMat);
        Imgproc.cvtColor(blackMat, blackMat, Imgproc.COLOR_RGB2GRAY);

        MatOfInt hull = new MatOfInt();
        boolean contourObtained = false;
        for (MatOfPoint contour : contours) {
            //Imgproc.convexHull(contour, hull);
            //contour = hull2Points(hull, contour);
            MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());

            // approximate polygon shape based on contours detected
            double peri = Imgproc.arcLength(contour2f, false);
            MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
            Imgproc.approxPolyDP(contour2f, matOfPoint2f, 0.02 * peri, true);
            //contour = new MatOfPoint(matOfPoint2f.toArray());
            // take the largest quadrilateral shape
            if (matOfPoint2f.toArray().length == 4 && Imgproc.contourArea(contour) > (cannyMat.rows() * cannyMat.cols()) / 7) {
                Imgproc.convexHull(contour, hull, false);
                List<MatOfPoint> matOfPoints = Collections.singletonList(contour);
                contourObtained = true;
                Imgproc.drawContours(blackMat, matOfPoints, -1, new Scalar(255), 1);
                break;
            }
        }

        if (contourObtained) {
            Utils.matToBitmap(blackMat, outlineBitmap);
            return blackMat;
        } else {
            Log.d(TAG, "failed to find contour");
            Utils.matToBitmap(cannyMat, outlineBitmap);
            return cannyMat;
        }
    }

    protected static DocumentScannerActivity.Quadrilateral detectHoughLines(Mat outlineMat) {
        Mat oriMat = new Mat();
        Utils.bitmapToMat(originalBitmap, oriMat);
        Mat lines = new Mat();
        Imgproc.HoughLines(outlineMat, lines, 1, Math.PI / 180, outlineMat.width() / 10);

        List<double[]> strongLinesData = new ArrayList<>();

        // detect the 4 non-overlapping lines with the highest confidence
        for (int i = 0; i < lines.rows(); i++) {
            double[] data = lines.get(i, 0);

            // account for negative angles
            if (data[0] < 0) {
                data[0] = Math.abs(data[0]);
                data[1] -= Math.PI;
            }

            double rho = data[0];
            double theta = data[1];
            double cosTheta = Math.cos(theta);
            double sinTheta = Math.sin(theta);
            double x0 = cosTheta * rho;
            double y0 = sinTheta * rho;
            Point pt1 = new Point(x0 + 10000 * (-sinTheta), y0 + 10000 * cosTheta);
            Point pt2 = new Point(x0 - 10000 * (-sinTheta), y0 - 10000 * cosTheta);

            if (strongLinesData.size() < 4) {
                boolean isStrongLine = true;
                for (double[] line : strongLinesData) {
                    double rhoThreshold = 20;
                    double thetaThreshold = Math.toRadians(30);

                    double rhoDiff = Math.abs(Math.abs(rho) - Math.abs(line[0]));
                    double thetaDiff = Math.min(Math.abs(theta - line[1]), Math.abs(Math.toRadians(180) - Math.abs(theta - line[1])));

                    if (rhoDiff < rhoThreshold && thetaDiff < thetaThreshold) {
                        isStrongLine = false;
                        break;
                    }
                }

                if (isStrongLine) {
                    strongLinesData.add(data);
                    Imgproc.line(oriMat, pt1, pt2, new Scalar(0, 255, 0), 1);
                }
            } else {
                break;
            }
        }

        // find the corner points using intersection of the 4 lines
        List<Point> src_pt = new ArrayList<>();
        for (int i = 0; i < strongLinesData.size(); i++) {
            double[] currentLineData = strongLinesData.get(i);
            DocumentScannerActivity.Line currentLine = lineFromData(currentLineData);
            int amt = 0;
            Log.d(TAG, "theta: " + currentLineData[1] + ", rho: " + currentLineData[0]);
            for (int j = i; j < strongLinesData.size(); j++) {
                double[] otherLineData = strongLinesData.get(j);
                DocumentScannerActivity.Line otherLine = lineFromData(otherLineData);
                //Log.d(TAG, "theta 1: " + currentLineData[1] + ", theta 2: " + otherLineData[1]);
                if (Math.abs(currentLineData[1] - otherLineData[1]) > (45 * Math.PI / 180) && Math.abs(currentLineData[1] - otherLineData[1]) < (135 * Math.PI / 180)) {
                    Point intersectionPt = intersection(currentLine, otherLine);
                    if (intersectionPt != null) {
                        amt++;
                        src_pt.add(intersectionPt);
                        Imgproc.drawMarker(oriMat, intersectionPt, new Scalar(255, 0, 0), Imgproc.MARKER_DIAMOND, 2, 2, Imgproc.LINE_4);
                    }
                }
            }
        }

        Utils.matToBitmap(oriMat, houghLinesBitmap);
        oriMat.release();
        return new DocumentScannerActivity.Quadrilateral(src_pt);
    }

    protected static Bitmap performPerspectiveCorrection(DocumentScannerActivity.Quadrilateral srcCorners, Size size) {
        Mat oriMat = originalMat.clone();
        List<Point> dst_pt = new ArrayList<>();
        dst_pt.add(new Point(0, 0));
        dst_pt.add(new Point(0, size.height));
        dst_pt.add(new Point(size.width, 0));
        dst_pt.add(new Point(size.width, size.height));
        DocumentScannerActivity.Quadrilateral dstCorners = new DocumentScannerActivity.Quadrilateral(dst_pt);

        Mat startM = Converters.vector_Point2f_to_Mat(srcCorners.sortedList);
        Mat endM = Converters.vector_Point2f_to_Mat(dstCorners.sortedList);
        Mat perspectiveTransform = Imgproc.getPerspectiveTransform(startM, endM);
        Imgproc.warpPerspective(oriMat, perspectiveMat, perspectiveTransform, size, Imgproc.INTER_CUBIC);

        Utils.matToBitmap(perspectiveMat, perspectiveBitmap);

        startM.release();
        endM.release();
        perspectiveTransform.release();
        oriMat.release();
        return perspectiveBitmap;
    }
}
*/
