package com.iservetech.documentocr;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by Ze Khan on 24-Jan-18.
 */

public class OcrDisclaimerDialog extends android.support.v4.app.DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Disclaimer on OCR usage")
                .setMessage(
                        "1)  OCR only performs well on surfaces that are not reflecting light.\n" +
                        "2)  OCR results might be affected by wear and tear as well as damages to the document\n" +
                        "3)  OCR helps to facilitate the process of information filling but may not guarantee a 100% accuracy. Hence, the user must always perform a manual check and perform correction, if necessary.")
                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onDialogPositiveClick("");
                        dismiss();
                    }
                })
                .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onDialogNegativeClick("");
                        dismiss();
                    }
                })
        .setCancelable(false);

        return builder.create();
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mListener = (DisclaimerDialogListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public interface DisclaimerDialogListener{
        public void onDialogPositiveClick(String string);
        public void onDialogNegativeClick(String string);
    }

    DisclaimerDialogListener mListener;
}
