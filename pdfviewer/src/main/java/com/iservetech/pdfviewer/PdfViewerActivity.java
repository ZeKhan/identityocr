package com.iservetech.pdfviewer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;

/**
 * Created by Ze Khan on 30-Jan-18.
 */

public class PdfViewerActivity extends AppCompatActivity{
    PDFView pdfView;
    static File mFile;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pdf_view);
        pdfView = findViewById(R.id.pdf_view);
        try {
            pdfView.fromFile(mFile).load();
        }catch (Exception e){e.printStackTrace();}
    }

    public static void openPdf(Activity activity, File file){
        mFile = file;
        Intent intent = new Intent(activity, PdfViewerActivity.class);
        activity.startActivity(intent);
    }

    public static void openPdf(Activity activity, Byte[] bytes){
        //mFile = file;
        Intent intent = new Intent(activity, PdfViewerActivity.class);
        activity.startActivity(intent);
    }
}
