package com.iserve.newlayout.facetracker;

import android.media.MediaPlayer;
import android.util.Log;

import com.google.android.gms.vision.face.Face;
import com.iserve.newlayout.R;

import java.util.Timer;
import java.util.TimerTask;

import static com.iserve.newlayout.facetracker.FaceTrackerActivity.faceImage;
import static com.iserve.newlayout.facetracker.FaceTrackerActivity.mContext;
import static com.iserve.newlayout.facetracker.FaceTrackerActivity.mListener;

/**
 * Created by Ze Khan on 22 January 2018.
 */

class FaceActionTracker {
    private static final String TAG = "actionDetector";
    private static int leftCount = 0, rightCount = 0;
    private static final int directionCount = 5;

    private enum Stage {Start, Left, Right, Blink, End}

    private static Stage mStage = Stage.Start;
    protected static int mFaceId;
    private static boolean firstRun;

    static void initActionTracker(int faceId) {
        mFaceId = faceId;
        changeStage(Stage.Start);
    }

    static MediaPlayer mp;

    private static void changeStage(Stage stage) {
        mStage = stage;
        switch (mStage) {
            case Start:
                firstRun = true;
                tracking = true;
                FaceTrackerActivity.showToast("Please start by looking straight to the camera");

                FaceTrackerActivity.mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (FaceTrackerActivity.frontCheck != null) {

                        FaceTrackerActivity.textFace.setText("Face Recognition : Front");
                        FaceTrackerActivity.frontCheck.setImageResource(R.drawable.custom_circle_grey);
                        FaceTrackerActivity.leftCheck.setImageResource(R.drawable.custom_circle_grey);
                        FaceTrackerActivity.rightCheck.setImageResource(R.drawable.custom_circle_grey);
                    }
                }
            });
                break;
            case Left:
                leftCount = 0;
                FaceTrackerActivity.showToast("Please start looking to the left");

                FaceTrackerActivity.mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (FaceTrackerActivity.frontCheck != null) {

                            FaceTrackerActivity.textFace.setText("Face Recognition : Left");
                            FaceTrackerActivity.frontCheck.setImageResource(R.drawable.custom_circle_primary);
                            FaceTrackerActivity.leftCheck.setImageResource(R.drawable.custom_circle_grey);
                            FaceTrackerActivity.rightCheck.setImageResource(R.drawable.custom_circle_grey);
                        }
                    }
                });

                break;
            case Right:
                rightCount = 0;
                FaceTrackerActivity.showToast("Please start looking to the right");

                FaceTrackerActivity.mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (FaceTrackerActivity.frontCheck != null) {

                            FaceTrackerActivity.textFace.setText("Face Recognition : Right");
                            FaceTrackerActivity.frontCheck.setImageResource(R.drawable.custom_circle_primary);
                            FaceTrackerActivity.leftCheck.setImageResource(R.drawable.custom_circle_primary);
                            FaceTrackerActivity.rightCheck.setImageResource(R.drawable.custom_circle_grey);
                        }
                    }
                });

                break;
            case End:
                //FaceTrackerActivity.showToast("Performing facial recognition");
                if (mListener != null) {
                    mListener.onFaceValidated(faceImage);
                }

                FaceTrackerActivity.mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (FaceTrackerActivity.frontCheck != null) {

                            FaceTrackerActivity.textFace.setText("Face Recognition : Done");
                            FaceTrackerActivity.frontCheck.setImageResource(R.drawable.custom_circle_primary);
                            FaceTrackerActivity.leftCheck.setImageResource(R.drawable.custom_circle_primary);
                            FaceTrackerActivity.rightCheck.setImageResource(R.drawable.custom_circle_primary);
                        }
                    }
                });


                FaceTrackerActivity.mActivity.finish();

                break;
        }
    }

    static void startTracking(boolean start) {
        tracking = start;
    }

    private static boolean tracking = false;

    private static Timer timer;
    private static float currentAngle;
    static void updateAction(Face face) {
        currentAngle = face.getEulerY();
        //Log.d(TAG, "current angle: " + face.getEulerY());
        if (tracking) {
            switch (mStage) {
                case Start:
                    if (currentAngle > -5 && currentAngle < 5) {
                        tracking = false;
                        FaceTrackerActivity.showToast("Please hold still as we take a picture");

                        if(timer != null) {
                            timer.cancel();
                        }

                        timer = new Timer();
                        timer.schedule(new TimerTask() {

                            @Override
                            public void run() {
                                if (FaceTrackerActivity.faceDetected) {
                                    Log.d(TAG, "Taking picture");
                                    FaceTrackerActivity.takePicture();
                                    changeStage(Stage.Left);
                                    if (mp != null) {
                                        mp.stop();
                                    }
                                    mp = MediaPlayer.create(mContext, R.raw.stay);
                                    mp.start();
                                }else{
                                    tracking = true;
                                }
                            }
                        }, 2000);

                    }
                    break;
                case Left:
                    if (currentAngle > 20) {
                        leftCount++;
                        if (leftCount > directionCount) {
                            changeStage(Stage.Right);
                            if (mp != null) {
                                mp.stop();
                            }
                            mp = MediaPlayer.create(mContext, R.raw.stay);
                            mp.start();
                        }
                    }
                    break;
                case Right:
                    if (currentAngle < -20) {
                        rightCount++;
                        if (rightCount > directionCount) {
                            changeStage(Stage.End);
                            if (mp != null) {
                                mp.stop();
                            }
                            mp = MediaPlayer.create(mContext, R.raw.stay);
                            mp.start();
                        }
                    }
                    break;
                case End:
                    tracking = false;
                    break;
            }
        }
        firstRun = false;
    }

}
