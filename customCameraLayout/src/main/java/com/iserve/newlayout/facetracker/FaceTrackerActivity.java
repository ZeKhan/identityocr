/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.iserve.newlayout.facetracker;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.gms.vision.face.LargestFaceFocusingProcessor;
import com.google.gson.Gson;
import com.iserve.newlayout.R;
import com.iserve.newlayout.facetracker.ui.camera.CameraSourcePreview;
import com.iserve.newlayout.facetracker.ui.camera.GraphicOverlay;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Activity for the face tracker app.  This app detects faces with the rear facing camera, and draws
 * overlay graphics to indicate the position, size, and ID of each face.
 */
public final class FaceTrackerActivity extends AppCompatActivity {
    private static final String TAG = "FaceTracker";
    public static Context mContext;
    public static Activity mActivity;
    private static CameraSource mCameraSource = null;

    static onFaceValidatedListener mListener;

    public static Bitmap faceImage;

    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    public static ImageView frontCheck, leftCheck, rightCheck;
    public static TextView textFace;

    //==============================================================================================
    // Activity Methods
    //==============================================================================================

    /**
     * Initializes the UI and initiates the creation of a face detector.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.facetrackeractivity);

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);
        textFace = (TextView) findViewById(R.id.textFace);
        frontCheck = (ImageView) findViewById(R.id.frontCheck);
        leftCheck = (ImageView) findViewById(R.id.leftCheck);
        rightCheck = (ImageView) findViewById(R.id.rightCheck);

        textFace.setText("Face Recognition : Front");
        frontCheck.setImageResource(R.drawable.custom_circle_grey);
        leftCheck.setImageResource(R.drawable.custom_circle_grey);
        rightCheck.setImageResource(R.drawable.custom_circle_grey);

        mContext = this;
        mActivity = this;

        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }

        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
        }
    }

    public static void startFaceTracking(Activity activity, onFaceValidatedListener listener){
        mListener = listener;
        Intent intent = new Intent(activity, FaceTrackerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }


    public interface onFaceValidatedListener {
        void onFaceValidated(Bitmap bitmap);
        void onFaceMatched(boolean success, String message);
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }

    private void createCameraSource() {
        Context context = getApplicationContext();
        FaceDetector detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .setMode(FaceDetector.ACCURATE_MODE)
                .setProminentFaceOnly(true)
                .setTrackingEnabled(true)
                .build();

        detector.setProcessor(new LargestFaceFocusingProcessor(detector, new GraphicFaceTracker(mGraphicOverlay)));

        if (!detector.isOperational()) {
            LowStorageDialog lowStorageDialog = new LowStorageDialog();
            lowStorageDialog.show(getSupportFragmentManager(), "lowStorageFragment");
            //showToast("Face detection library failed to download. Please ensure you have sufficient memory and is connected to the internet. This process is only required for the first time", Toast.LENGTH_LONG);
        }

        mCameraSource = new CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(640, 480)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(30.0f)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.blackColor));
            getWindow().setStatusBarColor(getResources().getColor(R.color.blackColor));
        }
        startCameraSource();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
    }

    public static void showToast(final String text) {
        showToast(text, Toast.LENGTH_SHORT);
    }

    static Toast currentToast;

    public static void showToast(final String text, final int length) {
        final Activity activity = mActivity;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (currentToast != null) {
                    currentToast.cancel();
                }
                if (length == 0 || length == 1) {
                    currentToast = Toast.makeText(activity, text, length);
                    currentToast.show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
            return;
        }

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Face Tracker sample")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    //==============================================================================================
    // Camera Source Preview
    //==============================================================================================

    private void startCameraSource() {

        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    static CameraSource.ShutterCallback mShutterCallback = new CameraSource.ShutterCallback() {
        @Override
        public void onShutter() {

        }
    };

    static CameraSource.PictureCallback mPictureCallback = new CameraSource.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] bytes) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 3;
            faceImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
            Log.d(TAG,"face image size: " + faceImage.getWidth() + "/" + faceImage.getHeight());
        }
    };

    public static void faceRecognition(Context context, Bitmap bitmap1, Bitmap bitmap2){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://101.99.65.70:41000/face_recognition";


        if(bitmap1 == null || bitmap2 == null){
            Log.d(TAG, "empty bitmap detected");
            return;
        }

        bitmap1 = optimalSize(bitmap1, 1000);
        bitmap2 = optimalSize(bitmap2, 1000);

        final String img1 = bitmap2Base64(bitmap1);
        final String img2 = bitmap2Base64(bitmap2);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                FaceMatchingResponse faceMatchingResponse = gson.fromJson(response, FaceMatchingResponse.class);
                String msg = "";
                float thresh1 = 0.5f,
                        thresh2 = 0.4f;

                if (faceMatchingResponse.distance > thresh1){
                    msg = "Face does not match";
                    faceMatchingResponse.success = false;
                }else if(faceMatchingResponse.distance > thresh2 && faceMatchingResponse.distance <= thresh1){
                    msg = "Average match";
                }else if(faceMatchingResponse.distance<= thresh2){
                    msg = "Great match";
                }

                if(!faceMatchingResponse.success){
                    //showToast("Failed to find at least one face in both image");
                }else{
                    //showToast("Face matching complete. " + msg);
                }

                if(mListener!=null){
                    mListener.onFaceMatched(faceMatchingResponse.success, msg);
                }
                Log.d("tag", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(mListener!=null){
                    mListener.onFaceMatched(false, "Failed to communicate with the server");
                }
                Log.d("tag","error");
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("img1",img1);
                params.put("img2",img2);
                return params;
            }

        };

        queue.add(stringRequest);
    }

    private static Bitmap optimalSize(Bitmap bitmap, int maxDimen){
        float r_width = (float)maxDimen/bitmap.getWidth();
        float r_height = (float)maxDimen/bitmap.getHeight();

        float ratio = Math.min(r_width, r_height);

        bitmap = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth() * ratio), (int)(bitmap.getHeight() * ratio), false);

        Log.d("tag","bitmap height: " + bitmap.getHeight() + ", width: " + bitmap.getWidth());

        return bitmap;
    }

    private static String bitmap2Base64(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();

        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static void takePicture() {
        mCameraSource.takePicture(mShutterCallback, mPictureCallback);
        FaceActionTracker.startTracking(true);

    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    Date disappearTime;
    Face disappearFace;
    static boolean faceDetected = false;

    protected class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;
        private int mFaceId;
        private Face latestFace;
        //private boolean faceDetected = false;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        @Override
        public void onNewItem(int faceId, Face item) {
            float timeDiff = 10;
            if (disappearTime != null) {
                Date currentTime = new Date();
                timeDiff = (currentTime.getTime() - disappearTime.getTime()) / 1000f;
            }

            if (timeDiff < 0.2f) {

            } else {
                mFaceId = faceId;
                mFaceGraphic.setId(faceId);
                FaceActionTracker.initActionTracker(faceId);
            }
            faceDetected = true;
        }


        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            latestFace = face;
            if (mFaceId == FaceActionTracker.mFaceId) {
                FaceActionTracker.updateAction(face);
            }
            //mOverlay.add(mFaceGraphic);
            //mFaceGraphic.updateFace(face);
        }

        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            if (faceDetected) {
                disappearTime = new Date();
                disappearFace = latestFace;
                faceDetected = false;
            }
            //mOverlay.remove(mFaceGraphic);
        }


        @Override
        public void onDone() {
            Log.d(TAG, "missing");
            if (mFaceId == FaceActionTracker.mFaceId) {
            }
            //mOverlay.remove(mFaceGraphic);
        }
    }

    public class FaceMatchingResponse{
        public boolean success;
        public float distance;
    }
}
