package com.iserve.newlayout;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by mrJaja on 24/1/2018.
 */

public class FaceRecognition extends AppCompatActivity {

    private View center;
    private ImageView captureButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mykadpreview);

        center = (View) findViewById(R.id.center);
        captureButton = (ImageView) findViewById(R.id.captureButton);

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Do capture || scanning image
                Toast.makeText(FaceRecognition.this, "Capture Image", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.blackColor));
            getWindow().setStatusBarColor(getResources().getColor(R.color.blackColor));
        }
    }

}
