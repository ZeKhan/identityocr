package com.iserve.newlayout;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.iserve.newlayout.texttracker.AutoFitTextureView;
import com.iserve.newlayout.texttracker.CameraFunction;

import java.io.File;

/**
 * Created by mrJaja on 24/1/2018.
 */

public class PassportPreview extends CameraFunction implements ActivityCompat.OnRequestPermissionsResultCallback {

    private ImageView captureButton, leftIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passportpreview);
        documentType = "Passport";

        ocrProgressBar = new ProgressDialog(this);

        cameraPreview = findViewById(R.id.camera_preview);
        mTextureView = (AutoFitTextureView) findViewById(R.id.texture);
        overlayView = (FrameLayout) findViewById(R.id.overlay_view);
        cropView = (View) findViewById(R.id.center);
        captureButton = (ImageView) findViewById(R.id.captureButton);
        leftIcon = (ImageView) findViewById(R.id.leftIcon);

        leftIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(PassportPreview.this, "Capture Image", Toast.LENGTH_LONG).show();
                takePicture();
            }
        });

        mFile = new File(getExternalFilesDir(null), "pic.jpg");

        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setActivecontext(this);

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.blackColor));
            getWindow().setStatusBarColor(getResources().getColor(R.color.blackColor));
        }

        startBackgroundThread();

        if (mTextureView.isAvailable()) {
            openCamera(mTextureView.getWidth(), mTextureView.getHeight());
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onPause() {
        //closeCamera();
        //stopBackgroundThread();
        super.onPause();
    }

}
