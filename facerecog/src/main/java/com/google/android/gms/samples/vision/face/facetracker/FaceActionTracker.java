package com.google.android.gms.samples.vision.face.facetracker;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.vision.face.Face;

import java.util.Timer;
import java.util.TimerTask;

import static com.google.android.gms.samples.vision.face.facetracker.FaceTrackerActivity.faceImage;
import static com.google.android.gms.samples.vision.face.facetracker.FaceTrackerActivity.mListener;

/**
 * Created by Ze Khan on 22 January 2018.
 */

class FaceActionTracker {
    private static final String TAG = "actionDetector";
    private static int leftCount = 0, rightCount = 0;
    private static final int directionCount = 10;

    private enum Stage {Start, Left, Right, Blink, End}

    private static Stage mStage = Stage.Start;
    protected static int mFaceId;
    private static boolean firstRun;

    static void initActionTracker(int faceId) {
        mFaceId = faceId;
        changeStage(Stage.Start);
    }

    private static void changeStage(Stage stage) {
        mStage = stage;
        switch (mStage) {
            case Start:
                firstRun = true;
                tracking = true;
                FaceTrackerActivity.showToast("Please start by looking straight to the camera");
                break;
            case Left:
                leftCount = 0;
                FaceTrackerActivity.showToast("Please start looking to the left");
                break;
            case Right:
                rightCount = 0;
                FaceTrackerActivity.showToast("Please start looking to the right");
                break;
            case End:
                FaceTrackerActivity.showToast("Performing facial recognition");
                if (mListener != null) {
                    mListener.onFaceValidated(faceImage);
                }
                FaceTrackerActivity.mActivity.finish();
                break;
        }
    }

    static void startTracking(boolean start) {
        tracking = start;
    }


    private static float prevAngle = 0f;
    private static boolean tracking = false;

    static void updateAction(Face face) {
        float currentAngle = face.getEulerY();
        Log.d(TAG, "current angle: " + face.getEulerY());
        if (firstRun)
            prevAngle = currentAngle;

        /*if(Math.abs(prevAngle - currentAngle) > 15){
            Log.d("face tracker", "Face turn too fast");
            changeStage(Stage.Start);
        }*/
        if (tracking) {
            switch (mStage) {
                case Start:
                    if (currentAngle > -5 && currentAngle < 5) {
                        tracking = false;
                        FaceTrackerActivity.showToast("Please hold still as we take a picture");

                        new Timer().schedule(new TimerTask() {
                            final int id = mFaceId;

                            @Override
                            public void run() {
                                if (FaceTrackerActivity.faceDetected) {
                                    Log.d(TAG, "Taking picture");
                                    FaceTrackerActivity.takePicture();
                                    changeStage(Stage.Left);
                                }
                            }
                        }, 2000);
                    }
                    break;
                case Left:
                    if (currentAngle > 20) {
                        leftCount++;
                        if (leftCount > directionCount) {
                            changeStage(Stage.Right);
                        }
                    }
                    break;
                case Right:
                    if (currentAngle < -20) {
                        rightCount++;
                        if (rightCount > directionCount) {
                            changeStage(Stage.End);
                        }
                    }
                    break;
                case End:
                    tracking = false;
                    break;
            }
        }
        firstRun = false;
    }

}
