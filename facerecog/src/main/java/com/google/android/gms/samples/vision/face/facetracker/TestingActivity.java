package com.google.android.gms.samples.vision.face.facetracker;

import android.media.FaceDetector;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

/**
 * Created by Ze Khan on 22 January 2018.
 */

public class TestingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.image);
        ImageView mImageView = (ImageView) findViewById(R.id.image_view);
        if (FaceTrackerActivity.faceImage != null) {
            mImageView.setImageBitmap(FaceTrackerActivity.faceImage);
        }
    }
}
